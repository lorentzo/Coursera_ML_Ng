
\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figure/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\tableofcontents
\newpage

\textbf{Week 1.}


\section{Linear regression with one variable}
Given the dataset, fit with straight line, e.g. figure \ref{linreg}

\begin{figure}[H]
  \includegraphics[scale=0.5]{linreg}
  \centering
  \caption{Example of linear regression fit}
  \label{linreg}
\end{figure}

Linear regression is example of \textbf{supervised learning}. That means pairs $(x^{(i)},y^{(i)})$ are given.
Also, linear regression is \textbf{parametric learning algorithm}. That means it has fixed number of parameters
$\theta$ which do not depend on number of examples.\\
Another example of supervised learning is \textbf{classification}. Difference between classification and regression
is prediction of discrete or continuous values.\\
Schema of supervised algorithm is in figure \ref{im:schema}.\\
\begin{figure}[H]
  \includegraphics[scale=0.5]{scheme}
  \centering
  \caption{Schema of supervised algorithm}
  \label{im:schema}
\end{figure}

Representation of linear regression with one variable:
\begin{equation}
h_{\theta}(x) = \theta_0 + \theta_1 x
\label{eq:univarlinreg}
\end{equation}
This is linear function of one variable x. Thus name is univariate linear regression.\\

\subsection{Cost function}
Model is parametrized with $\theta_0, \theta_1$. Using the dataset we need to find optimal parameters that
results in hypothesis that best fits the dataset. Size of dataset is $m$.\\
In other words we need to find $\theta_0, \theta_1$ which give $h_{\theta}(x^i)$ (prediction) as close as possible 
to $y^i$ (real value).\\
Formally we define:
\begin{equation}
min_{\theta_0, \theta_1} \frac{1}{2m} \sum_{i=1}^{m}(h_{\theta}(x^i) - y^i)^2
\label{eq:mincost}
\end{equation}

So we define cost function:
\begin{equation}
J(\theta_0, \theta_1) = \frac{1}{2m} \sum_{i=1}^{m}(h_{\theta}(x^i) - y^i)^2
\label{eq:costFunc}
\end{equation}
Cost function is called mean square error function and it defines accuracy of hypothesis. We are trying to find parameters 
$\theta$ to minimize that function. \\
Errors from each example to hypothesis are in figure \ref{err}.

\begin{figure}[H]
  \includegraphics[scale=0.5]{err}
  \centering
  \caption{Error of fit: "vertical" distance between data points and fitted line.}
  \label{err}
\end{figure}


\subsubsection{Intuition of cost function}
Different values of $\theta_0, \theta_1$ give different hypothesis. Accuracy is measured with cost function.\\
Plotting hypothesis and cost function we can see that we have quadratic function shape figure \ref{im:simplTheta}. \\
So, minimum value of cost function gives optimal $\theta_0, \theta_1$ which gives hypothesis that fit the given data best.\\

\begin{figure}[H]
  \includegraphics[scale=0.5]{simpleTheta}
  \centering
  \caption{Example of cost function with different $\theta_1$ and $\theta_0 = 0$}
  \label{im:simplTheta}
\end{figure}

\subsubsection{Probabilistic interpretation}
Now we will take probabilistic approach to explain why squared error function is chosen for cost function for linear regression.\\
First, we assume:
\begin{equation}
y^{(i)} = \theta^T x^{(i)} + \epsilon^{(i)}
\end{equation}
That means target value $y^{(i)}$ is generated with $\theta^T x^{(i)}$ added noise $\epsilon^{(i)}$.\\
Value $\theta$ exist and using given data we need to learn that parameter.\\
Value $\epsilon$ is noise, we model it as random variable. We assume: $\epsilon = N(0, \sigma^2)$. So, noise is 
distributed by normal distribution with mean 0 and variance $\sigma^2$.
\begin{equation}
p(\epsilon^{(i)}) = \frac{1}{\sqrt{2 \pi} \sigma}exp(-\frac{(\epsilon^{(i)})^2}{2 \sigma^2})
\end{equation}
This means that value $y^{(i)}$ is random variable distributed: $y^{(i)} | x^{(i)};\theta = N(\theta^T x^{(i)}, \sigma^2)$
\begin{equation}
p(y^{(i)} | x^{(i)};\theta) = \frac{1}{\sqrt{2 \pi} \sigma}exp(-\frac{(y^{(i)} - \theta^T x^{(i)})^2}{2 \sigma^2})
\end{equation}
Another assumption is $\epsilon^{(i)}$ is IID (every example is independent and distributed by same distribution).\\
We define \textbf{likelihood} $L(\theta)$ which stands for likelihood of parameters for given data. 
\begin{equation}
L(\theta) = p(\vec{y}|X, \theta) = \quad (IID) \quad = \prod_{i=1}^{m} p(y^{(i)}|x^{(i)};\theta)
\end{equation}
Likelihood is viewing function $p(\vec{y}|X, \theta)$ as function of  $\theta$. Now we want to find $\theta$ that
is most likely for data. \textbf{Maximum likelihood estimator}.\\
For easier calculation we use $l(\theta) = log(L(\theta))$:
\begin{equation}
l(\theta) = \sum_{i=1}^{m} log (p(y^{(i)}|x^{(i)};\theta))
\end{equation}
And:
\begin{equation}
max_{\theta}l(\theta) \quad is \quad min \frac{1}{2} \sum_{i=1}^{m} (y^{(i)} - \theta^T x^{(i)})^2 = J(\theta)
\end{equation}
\textbf{NB:} $\sigma^2$ is not influencing the minimization.
So, we minimize $J(\theta)$ using gradient descend which is also maximizing likelihood of parameters $\theta$
for given data.\\


\subsection{Parameter learning}
We will use gradient descent to find optimal $\theta_0, \theta_1$, general idea:
\begin{itemize}
\item[1] initialise $\theta_0, \theta_1$
\item[2] simultaneously minimise $\theta_0, \theta_1$

\begin{equation}
tmp0 = \theta_0 - \alpha \frac{\partial J(\theta_0, \theta_1)}{\partial \theta_0} 
\label{eq:grad0}
\end{equation}

\begin{equation}
tmp1 = \theta_1 - \alpha \frac{\partial J(\theta_0, \theta_1)}{\partial \theta_1} \\
\label{eq:grad1}
\end{equation}

\begin{equation}
\theta_0 = tmp0
\label{eq:grad3}
\end{equation}

\begin{equation}
\theta_1 = tmp1
\label{eq:grad4}
\end{equation}

\end{itemize}


\subsection{Intuition for gradient descend}
Negative value of gradient gives direction of minimum. Gradient at (local) minimum gives value $0$.  \\
Constant: $\alpha$ is called learning rate and it controls step in direction of gradient.\\
Small value $\alpha$ results in slow convergence, and slow algorithm.\\
Large value of $\alpha$ results in not convergence or even divergence.\\
Gradient descend can converge to minimum with fixed $\alpha$ value because gradient is automatically
take smaller steps as it approaches the minimum

\subsection{Result of gradient descend applied to cost function}
Solving the equation \ref{eq:grad0} and \ref{eq:grad1}, where cost function is \ref{eq:costFunc} gives following
expression for learning parameters of univariate linear regression:

\begin{equation}
tmp0 = \theta_0 - \alpha \frac{1}{m} \sum_{i=1}^{m}(h_{\theta}(x_i) - y_i)
\label{eq:gradsol0}
\end{equation}

\begin{equation}
tmp0 = \theta_0 - \alpha \frac{1}{m} \sum_{i=1}^{m}(h_{\theta}(x_i) - y_i) x_i
\label{eq:gradsol1}
\end{equation}

Cost function \ref{eq:costFunc} is convex so it has only one minimum.\\
This version of gradient descend in every iteration take whole dataset and it is called batch learning. Using one
example per iteration is called online learning.\\
Another way to find optimal parameters is using normal equation which is not iterative method.\\


\textbf{Week 2.}

\section{Multi variable linear regression}
Number of features is $n>1$.\\
We denote $x^(i)_j$ $ith$ example (row) and $jth$ value of example (row).\\

\subsection{Form of hypothesis}
For $n>1$ we have:
\begin{equation}
h_\theta(x) = \theta_0 x_0 + ... + \theta_n x_n = \sum_{j=1}^{n} \theta_j x_j
\label{eq:multivarlinreg}
\end{equation}
Where $x_0=1$, $x_0{^(i)} = 1$.\\
Vector representation:
\begin{equation}
h_\theta(x) = \begin{bmatrix} \theta_0 & ... & \theta_1 \end{bmatrix} \begin{bmatrix} x_0 \\ ... \\ x_1 \end{bmatrix} = \theta^T x
\label{eq:mvlrvector}
\end{equation}

\subsection{Gradient descend}
Now, cost function is:
\begin{equation}
J(\theta_0, \theta_1, ..., \theta_n) = J(\vec{\theta}) = \frac{1}{2m} \sum_{i=1}^{m}(h_{\theta}(x^i) - y^i)^2
\label{eq:costFuncMulti}
\end{equation}
Where $h_{\theta}(x^i)$ is in equation \ref{eq:mvlrvector} or \ref{eq:multivarlinreg}.\\
Gradient descent:
\begin{equation}
tmp_j = \theta_j - \alpha \frac{\partial J(\vec{\theta})}{\partial \theta_j} 
\label{eq:gradMulti0}
\end{equation}

\begin{equation}
\theta_j = tmp_j
\label{eq:gradMulti0}
\end{equation}

Solving gives:

\begin{equation}
tmp_j = \theta_j - \alpha \frac{1}{m} \sum_{i=1}^{m}(h_{\vec{\theta}}(x_i) - y_i) x_j^{(i)}
\label{eq:gradsol1}
\end{equation}

\subsubsection{Feature scaling and mean normalization}
We want similar range of feature values.\\
Rule of thumb: not more than $-3 \leqslant x_i \leqslant 3$ and not less than $-1/3 \leqslant x_i \leqslant 1/3$. \\
If values of features are in similar range then gradient descend converges more quickly, contours of cost function are
more circular, see figure \ref{im:range}.\\
\begin{figure}[H]
  \includegraphics[scale=0.5]{badgoodrange}
  \centering
  \caption{Left: features not in similar range. Right: features in similar range.}
  \label{im:range}
\end{figure}
So we can see that feature range directly influence parameters.\\

Solutions:
\begin{itemize}
\item[1] Scaling: 
\begin{equation}
x_i = \frac{x_i}{S}
\end{equation}
Where S is range: difference between maximum and minimum value or standard deviation.\\
\item[2] Mean normalization
\begin{equation}
x_i = \frac{x_i - \mu_i}{S}
\end{equation}
Where $\mu_i$ is mean of feature values.\\
\end{itemize}

Note: once optimal parameters are found and before predicting unknown example, that example
must be normalised using found mean and spread on train data.\\

\subsection{Learning rate}
For validating learning rate $\alpha$ plot $J(\vec{\theta})$ - $\# of iterations$ is used. If value
$J(\vec{\theta})$ decreases in every iteration then we chose good $\alpha$, figure \ref{im:J}.\\
\begin{figure}[H]
  \includegraphics[scale=0.5]{JnoI}
  \centering
  \caption{Example of cost function changing with iterations}
  \label{im:J}
\end{figure}
On the other hand $\alpha$ shouldn't be too small - results in slow convergence.
Another approach is automatic convergence test. Small value $\epsilon = 10^-3$ is set and $\alpha$ 
value is good if decrease is larger than $\epsilon$.\\
Rule of thumb, values of $\alpha={0.001, 0.003, 0.01, 0.03, 0.1, 0.3,...}$ and find largest without
divergence.\\

\subsection{Linear regression with higher polynomial and other functions features}
If, for example, features are $x_1 - width$ and $x_2 - height$ then standard representation is:
\[ h_\theta(x) = \theta_0 + \theta_1 x_1 + \theta_2 x_2 \]
New feature can be created: $x = x_1 x_2 - area$ then representation is:
\[ h_\theta(x) = \theta_0 + \theta_1 x \]
So, polynomial regression can be created. If $x - size$, then:
\[ h_\theta(x) = \theta_0 + \theta_1 x + \theta_2 x^2\]
Feature scaling is important because range of features are very different!\\
Another example:
\[ h_\theta(x) = \theta_0 + \theta_1 x + \theta_2 x^2 + \theta_3 x^3 \]
Also:
\[ h_\theta(x) = \theta_0 + \theta_1 x + \theta_2 \sqrt{x} \]
TODO - figures of functions\\

\subsection{Locally weighted linear regression - lowess}
If we use complex linear regression model (e.g. high order polynomial features) then we have problem
of \textbf{over fitting} - low train error, large generalization error. \\
If we use too simple linear regression model we have problem of \textbf{underfitting} - high train and
generalization error.\\
Another way of defining model without too much worrying about features is defining locally weighted
linear regression. This is \textbf{non parametric learning algorithm}. This means that number of parameters
is not fixed and it depends on number of examples $m$.\\
Idea of locally weighted linear regression is:
\begin{figure}[H]
\includegraphics[scale=0.5]{lwlr}
\centering
\caption{Example of locally weighted linear regression. Only subset of examples close to new example are
			fitted.}
\label{im:lwlr}
\end{figure}
Difference from linear regression is that it only fits locally - close to new example.\\
Therefore we fit parameters $\theta$ using:
\begin{equation}
min \sum_{i}^m \omega^{(i)} (y^{(i)} - \theta^Tx^{(i)})^2
\end{equation}
Where $\omega^{(i)}$ is "keeping" examples close to $x^{(i)})$:
\begin{equation}
\omega^{(i)} = exp(-\frac{(x^{(i)}-x)^2}{2 \tau^2})
\end{equation}
This means if $|x^{(i)}-x|$ small then $\omega^{(i)}=1$ otherwise if $|x^{(i)}-x|$ large then $\omega^{(i)}=0$.\\
$\tau$ defines bandwidth (width of bell shaped function $\omega$).\\
Using locally weighted linear regression for multiple points results in non linear fit line.\\


\subsection{Analytical computing of parameters}
Gradient descend is iterative method. Following method is one step method.\\

\subsubsection{Normal equation}
For m examples $(x^{(i)}, y^{(i)})$ and n features:
\[ x^{(i)} = \begin{bmatrix} x_0 \\ ... \\ x_n \end{bmatrix}. \]
Dimensions: $(n+1)x1$. Where $i = 1..m$.
We construct:
\[ X = \begin{bmatrix} --- (x^{(1)})^T --- \\ ... \\ --- (x^{(m)})^T --- \end{bmatrix} \]
Dimensions: $mx(n+1)$.\\
And:
\[ \vec{y} = \begin{bmatrix} y_0 \\ ... \\ y_m \end{bmatrix}. \]
Apply:
\[ \vec{\theta} = (X^TX)^{-1} X^T \vec{y} \]

Normal equation characteristics: 
\begin{itemize}
\item no need to choose learning rate $\alpha$
\item not iterative
\item for large number of features calculation $(X^TX)^{-1}$ very slow.
\end{itemize}

Gradient descend characteristics:
\begin{itemize}
\item find optimal learning rate $\alpha$
\item many iterations
\item good for large number of features
\end{itemize}

Problems of $(X^TX)$ inversion:
It is not invertible if singular or degenerate. That is because of:
\begin{itemize}
\item linearly dependent features (redundant features). Calculate correlation.
\item too many features and too small number of examples $m << n$. In this case features must 
	  be deleted or regularization should be preformed.
\end{itemize}

\subsubsection{Importance of vectorization}
Calculations are more  faster if data is processed in vector forms not using loops.\\
Equation \ref{eq:mvlrvector} is example of vectorization.\\
Also, equation \ref{eq:costFuncMulti} can be written as:
\begin{equation}
J(\vec{\theta}) = \frac{1}{2m}(X \vec{\theta} - \vec{y})^T ( X\vec{\theta} - \vec{y})
\label{eq:vectorCost}
\end{equation}
Also, equation \ref{eq:gradsol1} can be written as:
\begin{equation}
\vec{\theta} = \vec{\theta} - \alpha \vec{\delta}
\end{equation}
Where $\vec{\delta}$ and $\vec{\theta}$ are vectors of $(n+1)$ elements. See code for more explanation.\\



 




\end{document}


\contentsline {section}{\numberline {1}Linear regression with one variable}{2}
\contentsline {subsection}{\numberline {1.1}Cost function}{3}
\contentsline {subsubsection}{\numberline {1.1.1}Intuition of cost function}{3}
\contentsline {subsubsection}{\numberline {1.1.2}Probabilistic interpretation}{4}
\contentsline {subsection}{\numberline {1.2}Parameter learning}{5}
\contentsline {subsection}{\numberline {1.3}Intuition for gradient descend}{5}
\contentsline {subsection}{\numberline {1.4}Result of gradient descend applied to cost function}{5}
\contentsline {section}{\numberline {2}Multi variable linear regression}{6}
\contentsline {subsection}{\numberline {2.1}Form of hypothesis}{6}
\contentsline {subsection}{\numberline {2.2}Gradient descend}{6}
\contentsline {subsubsection}{\numberline {2.2.1}Feature scaling and mean normalization}{7}
\contentsline {subsection}{\numberline {2.3}Learning rate}{7}
\contentsline {subsection}{\numberline {2.4}Linear regression with higher polynomial and other functions features}{8}
\contentsline {subsection}{\numberline {2.5}Locally weighted linear regression - lowess}{9}
\contentsline {subsection}{\numberline {2.6}Analytical computing of parameters}{10}
\contentsline {subsubsection}{\numberline {2.6.1}Normal equation}{10}
\contentsline {subsubsection}{\numberline {2.6.2}Importance of vectorization}{11}

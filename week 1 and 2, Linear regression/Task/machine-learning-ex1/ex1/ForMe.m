%% What I have learned in linear regression
%First part is univariate linear regression
%second part is multivariate linear regression


%% PART I: univariate linear regression

%% 1. Reading data
%X - examples (add the intercept term 1)
%y - class
data = load('file.txt'); %also csvread()
y = data(:, 2);
X = data(:, 1); 
X = [ones(m, 1), data(:,1)]; % Add a column of ones to x

%% 2. Plotting 1d data pairs(x,y) - scatter plot
figure;
plot(x,y,'b*','MarkerSize',12);
ylabel('Profit in $10,000$');
xlabel('Population of city in 10,000$');

%% 3. Cost function 1d data
m = length(y);
squared_err = (X * theta - y) .^ 2; %(n+1) vector
squared_err = sum(squared_err);
squared_err = (1/(2*m)) * squared_err;
J = squared_err;

%% 4. Grad descend 1d data
for iter = 1:num_iters
    err = X * theta - y;
    err = X' * err;
    theta = theta - alpha * (1/m) * err;
end


%% PART II. multivariate linear regression (n >= 2)

%% 1. Reading data and normalization
data = load('file.txt');
X = data(:, 1:2);
y = data(:, 3);

%mean over features (columns)
mu = mean(X,1);
%substract by mean
X_norm = X - ones(size(X)) .* mu;

%std over features (comulns). Biased (divide by m) ALT: max - min
sigma = std(X, 0, 1);
%divide by sigma
X_norm = X_norm ./ repmat(sigma, size(X,1), 1);
%all values [-2,2]

% Add intercept term to X after normalization!!!
X = [ones(m, 1) X];

%IMPORTANT: after learning parameters, before predicting, new values must
%be normalised with found mean and var from TRAINING dataset!

%% 2. Gradient descent for finding parameters = minimising cost function

%cost function
m = length(y);
squared_err = (X*theta - y) .^ 2;
squared_err = (1 / (2*m)) * sum(squared_err);
J = squared_err;

%grad desc
for iter = 1:num_iters
err = X * theta - y;
err = X' * err;
theta = theta - alpha * (1/m) * (err);
end


%% 3. Predicting values using multivariate lin reg
% Recall that the first column of X is all-ones. Thus, it does
% not need to be normalized.
x = [1650 3];
%normalise
x = (x - mu) ./ sigma;
%add intercept
x = [1, x]';
disp(x);
%predict using optimal theta
h = theta' * x;

%% 4. Normal equation and prediction
theta = inv(X' * X) * X' * y;
%no need for normalization!
x = [1 1650 3]';
price = theta' * x;
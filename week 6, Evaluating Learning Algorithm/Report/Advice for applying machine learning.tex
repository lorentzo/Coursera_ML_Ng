\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\textbf{Week 6. Advice for applying machine learning}

\section{Evaluation of a learning algorithm}

\subsection{Debugging learning algorithm}
Using linear regression, logistic regression or neural networks, by minimising cost function we find optimal parameters
(hypothesis) that fits train data.\\
What to do if error of that hypothesis is large on test set?
\begin{itemize}
\item More training data?
\item Smaller set of features?
\item Finding additional features from data?
\item adding polynomial features?
\item change regularization factor?
\end{itemize}
Using \textbf{machine learning diagnosis} we can figure out what is/isn't working and get guidance how to improve 
performance. Diagnosis requires time but is very useful.\\

\subsection{Evaluating hypothesis}
It is important to figure out if hypothesis is overfitting (high variance): having low train and high test error.
Or hypothesis is underfitting (high bias): having low train and test error.\\
For one feature it is possible to plot hypothesis and data to infer what is wrong.\\
For high dimensional data (more than one feature), method of splitting data is used: train ($80\%$) and test ($20\%$).\\
\textbf{Procedure for linear regression:}\\
\begin{itemize}
\item[1] Learn parameters $\theta$ my minimising cost function $J_{train}(\theta)$ using training set.
\item[2] Calculate test set error by calculating cost function $J_{test}(\theta)$.\\
\end{itemize}
Where:
\begin{equation}
J(\vec{\theta}) = \frac{1}{2m} \sum_{i=1}^{m}(h_{\theta}(x^i) - y^i)^2
\end{equation}
\textbf{Procedure for logistic regression:}\\
\begin{itemize}
\item Learn $\theta$ from train set by minimising $J_{train}(\theta)$
\item Calculate test set error by calculating average misclassification error.\\
\end{itemize}
Misclassification error:
\begin{equation}
err(h_{\theta}, y) =
\begin{cases}
 1, \quad IF \quad h_{\theta}(x) \geq 0.5 \quad AND \quad y = 0 \quad 
OR \quad h_{\theta}(x) < 0.5 \quad AND \quad y = 1
\\
0, \quad otherwise 
\end{cases}
\end{equation}
Average misclassification error on test set:
\begin{equation}
AvgMisclasErr = \frac{1}{m} \sum_{i=1}^m err(h_{\theta}(x^{(i)}, y^{(i)})
\end{equation}

\subsection{Model selection}
Splitting data in test and train was useful for assessing hypothesis error on unseen data. In that case 
only parameters $\theta$ were trained and tested.\\
For some algorithms hyper-parameters also need to be chosen. Hyper-parameters are for example degree of
polynomial $d$ or regularization factor $\lambda$. These parameters must be chosen manually (more or less).\\
So for selecting these parameters data should be split into: Train set ($60\%$), Cross validation (CV) set ($20\%$)
and test set ($20\%$).\\
Procedure for choosing polynomial degree $d$ is:
\begin{itemize}
\item[1] Create array of desired polynomial degree $d$, let's say array size of $D$ different polynomial values)
\item[2] Optimize parameters $\theta$ for each polynomial degree. Now we have $D$ different hypothesis.
\item[3] Use CV set to find hypothesis (with polynomial degree $d$) with least error.
\item[4] Estimate generalization error for chosen hypothesis on test set, calculating cost function.
\end{itemize}
Cv set is needed because polynomial degree is not biased on data.\\

\section{Bias VS variance}

\subsection{Diagnosis: Bias VS variance}
How to figure out if hypothesis is underfitting (high bias) or overfitting (high variance) due to e.g. polynomial degree?\\
Idea is to plot error on train and CV/test set against degree value $d$. General idea is in figure \ref{im:diagnosis}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{diagnosis}
\centering
\caption{Train and CV/test error for different degree value $d$}
\label{im:diagnosis}
\end{figure}
\begin{itemize}
\item High bias, underfit: train and CV/test error are high
\item High variance, overfit: train error is low, CV/test error is high
\end{itemize}

\subsection{Regularization: Bias VS variance}
In case of high polynomial, using regularization could be used to find hypothesis that generalizes better.\\
\begin{itemize}
\item Large $\lambda$ result in biased hypothesis. Large polynomials are restrained and hypothesis is simple.
\item Low $\lambda$ result in variance hypothesis. Large polynomials are resulting in high complexity.
\end{itemize}
\textbf{Procedure for choosing $\lambda$:}\\
\begin{itemize}
\item[1] Create list of lambda values $\{0,0.01,0.02,0.04,0.08,0.16,0.32,0.64,1.28,2.56,5.12,10.24, ...\}$. Let's say
			$\Lambda$ different values.
\item[2] Choose set of models with different polynomial degree $d$ ($D$ different polynomial values (or other parameters). 
			Now we have $D$ different models.
\item[3] For each $\lambda$ learn parameters $\theta$ of each model. Now we have $D * \Lambda$ hypothesis.
\item[4] Using CV find which hypothesis out of $D * \Lambda$ gives least error calculating cost function without regularization.\\
\item[5] Asses generalization error of best hypothesis using test set.\\
\end{itemize}

\subsection{Learning curves: Bias VS variance}
Another way to diagnose bias and variance is plotting train and CV/test error against number of examples $m$, figure \ref{im:diagM}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{diagM}
\centering
\caption{Train and train/CV error against number of examples $m$}
\label{im:diagM}
\end{figure}
\begin{itemize}
\item With small number of examples train error is low because hypothesis is fitting all example perfectly. But, CV/test error is large
because hypothesis didn't learned much on small number of examples.
\item For larger number of examples train error is larger because hypothesis can't fit all examples perfectly. But, CV/test error
is smaller because hypothesis has learned more.
\item After certain number of examples curves will plateau out. This is example where more examples doesn't solve problem of
large generalization error.
\end{itemize}

\textbf{Problem of high bias.}\\
\begin{figure}[H]
\includegraphics[scale=0.5]{diagMBias}
\centering
\caption{Train and test/CV error against number of examples $m$, when bias large}
\label{im:diagMBias}
\end{figure}
\begin{itemize}
\item For small number of data train error is small, CV/Test error is large.
\item Train curve is having larger and larger error because hypothesis is too simple to fit the data
\item CV/Test curve is dropping because hypothesis is learning more
\item In plateau region (large number of examples) error is high for train and CV/Test curve. Small gap between CV/Test curve and train curve. 		More examples will not help! More complex hypothesis is needed
\end{itemize}

\textbf{Problem of high variance.}\\
\begin{figure}[H]
\includegraphics[scale=0.5]{diagMVariance}
\centering
\caption{Train and test/CV error against number of examples $m$, when variance large}
\label{im:diagMVariance}
\end{figure}
\begin{itemize}
\item For small number of data train error is small. CV/test error is large
\item large gap between train and CV/test curve .For large number of examples train error is small, CV/test error is large
\item For more examples train error is rising and CV/test error is dropping. Distance significant.
\item More examples will help. Restraining hypothesis will also help.
\end{itemize}

\textbf{Example of learning curves for regularized linear regression}.\\
\begin{itemize}
\item[1] For every number of examples in train set (1..m) calculate parameters $\theta$.
		 Use regularization if needed.
\item[2] Calculate train error (cost function without regularization on training data) 
			but only on selected number of examples from train dataset.
\item[3] Calculate CV error (cost function without regularization) using learned 
		parameters on whole CV dataset. 
\end{itemize}

\subsection{Decision}
\begin{itemize}
\item More examples: fix high variance
\item smaller set of features: fix high variance
\item adding features: fix bias
\item adding polynomial features: fix bias
\item decrease $\lambda$: fix bias
\item increase $\lambda$: fix variance
\end{itemize}

\textbf{Diagnosing NN}.\\
\begin{itemize}
\item Small NN: small number of hidden layers and small number of nodes in hidden layers - small number of parameters.
		Prone to underfitting: high bias. Computationally cheaper.
\item Large NN: large number of hidden layers and large number of nodes in hidden layers - large number of parameters.
		Prone to overfitting: high variance. Computationally expensive.
\end{itemize}
One hidden layer is default start. More hidden layers should be tested with cross validation set.\\
\\
\textbf{Model complexity effects.}\\
\begin{itemize}
\item Low order polynomial - low complexity - underfit - high bias
\item High order polynomial - high complexity - overfit - high variance
\end{itemize}
Perfect model: in between of this extremes.\\

\section{Machine learning system design}
\begin{itemize}
\item To optimize machine learning algorithm find where imporvements can be made. 
\item Evaluate different parts of machine learning. 
\item How to deal with skewed data.
\end{itemize}

\subsection{Example Building spam classifier}
\subsubsection{Prioritize on what to work on}
As this problem is supervised learning we need to define:
\begin{itemize}
\item[1] Features $x$. Choose $n$ words that will represent if mail is spam or non-spam. Common way is to 
		take 10000-50000 most common words. Then scan through email and see which words appear. Create 
		binary vector for every mail. Denote $x_j = 1$ if word $j$ appears.
\item[2] Class $y$ is: $0$ - non-spam, $1$ - spam
\end{itemize}
How to obtain low error?
\begin{itemize}
\item Collect lot of data (emails, honypot example). This will not help always even if $n$ is high!
\item Develop more sophisticated features (e.g. routing information)
\item Use additional algorithms to process input in different way (e.g. spell checking)
\end{itemize}
To help decide on what to focus we will introduce methods in next chapter.

\subsubsection{Error analysis}
To decide on what to focus on try:
\begin{itemize}
\item[1] Start by simple algorithm (quick and dirty). Test on CV dataset.
\item[2] Plot learning curves. This will show if we have a problem with high bias or high variance. And see if more data or more features is needed.
\item[3] Analyise which examples with highest errors and try to spot pattern. Idea is to examine misclassified examples, find categories of those examples and 
		find which category has most examples with errors. for categories with most misclassified examples try to find better features.
\end{itemize}

\textbf{Importance of numerical evaluation}
For deciding if some additional methods would help in classification we need one real number evaluation to compare effect of having and not having that 
additional methods. For example if stemming method would help. We need to try and evaluate. Later we will introduce this numerical evaluation methods.

\subsection{Error metrics}
Simplest error metric is accuracy:
\begin{equation}
A = \frac{correctly \quad predicted}{all \quad examples}
\end{equation}
\textbf{Problem of skewed data}\\
If data has one dominant class then algorithm that preforms classifying of every example in dominant class will be evaluated with high accuracy.\\
This is why other metrics should be used.\\
\\
\textbf{Precision and recall}\\
Idea is to construct confusion matrix as in figure \ref{im:confusion}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{confusion}
\centering
\caption{confusion matrix of two classes}
\label{im:confusion}
\end{figure}
\textbf{Precision}: of all examples where $y=1$ is predicted, what fraction actually has 1?\\
\begin{equation}
P = \frac{TP}{TP + FP}
\end{equation}
\textbf{Recall}: of all examples where $y=1$, which fraction is correctly predicted?\\
\begin{equation}
R = \frac{TP}{TP+FN}
\end{equation}
\\
\textbf{Tradeoff: precision VS recall}\\
In case of logistic regression: $h_\theta(x) \geq T$ where $T$ is threshold.\\
If $T$ is larger than $0.5$ then we try to avoid $FP$ case and precision is higher and recall is lower.\\
If $T$ is lower than $0.5$ then we try to avoid $FN$ case and precision is lower and recall is higher.\\
This is shown on figure \ref{im:RvsP}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{RvsP}
\centering
\caption{Recall vs precision with different threshold $T$}
\label{im:RvsP}
\end{figure}
To obtain only one number from precision and recall use \textbf{$F_1$ score}.\\
\begin{equation}
F_1 = 2\frac{PR}{P + R}
\end{equation}
Both precision and recall high will result in high $F_1$ score.\\
CV dataset can be used to find best threshold which maximises $F_1$ score.\\

\subsection{Data for machine learning}
Experiment with different learning algorithms using different dataset sizes. Test show that many learning
algorithms result better with bigger data.\\
Before choosing bigger dataset be certain that human expert can classify examples with choosen
set of features.\\
\textbf{For large dataset}
\begin{itemize}
\item[1] Use algorithm with many parameters (logistic regression with many features, neural 
		network with many hidden units. And we don't use regularization). This ensures low bias. in other words train error will be low.
\item[2] With huge train set (unlikely to overfit). So train error should be close to train error. In the end that means test error will be low.
\end{itemize}






















































\end{document}
function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

%% Cost function

%Calculate prediction 
h = (theta' * X')'; %[1x2] x [2x12] -> [1x12]

%squared error
se = (y - h) .^ 2; %[1x12]

%sum of squared err
J = (1/(2 * m)) * sum(se); %[1x1]

%regularization factor, do not regularize theta_0!
r = (1 / (2 * m)) * lambda * sum(theta(2:end) .^ 2);

%Regularized cost function
J = J + r;


%% Gradient
%prediction
h = (theta' * X')'; %[1x2] x [2x12] -> [1x12]' -> 12x1

%error
err = h - y; %[12x1]

%multiplication with x from derivative
grad = err' * X; %[1x2]

%sum of err
grad = (1/m) * grad; %[1x2]
grad = grad'; %[2x1]

%regularization factor (without intercept)
r = (lambda / m) * theta(2:end); 
grad = grad + [0; r];
% =========================================================================

grad = grad(:);

end

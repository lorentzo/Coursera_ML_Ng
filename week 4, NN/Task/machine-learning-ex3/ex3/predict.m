function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

%Adding intercept column to X (5000x400) -> (5000x401)
X = [ones(size(X,1),1), X];

fprintf('\nTheta1 x XT: (%d %d) x (%d %d)\n', size(Theta1, 1), size(Theta1, 2), size(X',1), size(X',2));

%Mapping from a(1) = x to a(2) (25x401) x (401x5000) = (25x5000)
z2 = Theta1 * X';
a2 = sigmoid(z2);
fprintf('\na2: (%d %d)\n', size(a2,1), size(a2,2));

%Adding intercept column to a2 (26x5000)
a2 = a2';
a2 = [ones(size(a2,1),1), a2];
a2 = a2';

fprintf('\nTheta2 x a2: (%d %d) x (%d %d)\n', size(Theta2, 1), size(Theta2, 2), size(a2,1), size(a2,2));
%Mapping from a(2) to a(3) = h (10x26) x (26x5000)
z3 = Theta2 * a2;
a3 = sigmoid(z3);

%Transpose a3 to get 5000x10
a3 = a3';

%arg max gives class with highest prob
[~, p] = max(a3, [], 2);

% =========================================================================


end

\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\textbf{Week 5.}

\section{Cost function}
Given examples $\{x^{(i)}, y^{(i)}\}$, where $i=1..m$. Number of layers $L$. Number of unites in layer $l$
denote as $s_l$.\\
\textbf{Binary classification.}\\
Class is from set $y \in \{0,1\}$. On other words $K=2$. We have only one output unit $h_{\theta}(x)$.\\
\textbf{Multi class classification.}\\
Class is more than 2 classes, $K \geq 3$. Number of units in output layer is $K$. Technique OVA (one-vs-all) is 
used to classify new example.\\
One hot encoding is used. To index $k-th$ output we write $(h_{\theta}(x))_k)$. So, output $h_{\theta}(x)$ is
vector of $K$ values.\\
\textbf{Cost function.}\\
Generalization of logistic regression cost function.\\
\begin{equation}
J(\theta) = - \frac{1}{m} \sum_{i=1}^{m} \sum_{k=1}^K [y_k^{(i)} log(h_{\theta}(x^{(i)})_k) + 
(1 - y_k^{(i)}) * log(1-h_{\theta}(x^{(i)})_k)] +
\frac{\lambda}{2m} \sum_{l=1}^{L-1} \sum_{i=1}^{s_l} \sum_{j=1}^{s_l+1} (\Theta_{j,i}^{l})^2
\end{equation}
Where $h_{\theta}(x^{(i)})$ are calculated using \textbf{Forward propagation.}\\
Also, do not add intercept in regularization sum.\\
The first two sums loop through number of output nodes. So, it adds up logistic regression costs calculated 
for each cell in the output layer.\\
Regularization part adds up the squares of all individual $\Theta$s in entire network. Fist sum is for all
layers, second two sums are for every matrices $\Theta$ (dimension is number of nodes in current layer
including bias and number of nodes in next layer excluding bias).\\

\section{Back propagation}
To minimise cost function and thus find best parameters that fit data we need to calculate gradients. So minimization 
can be ether gradient descend or some advanced methods that require gradient information.\\
Gradient is calculated with back propagation algorithm that is following:
\begin{itemize}
\item[0] Training set $\{x^{(i)}, y^{(i)}\}$, where $i=1..m$. For multi class we use one-hot-encoding vectors for class encoding.
\item[1] Set $\Delta_{i,j}^{(l)} = 0$ (for all $l,i,j$)
\item[2] for $t = 1$ to $m$:
\item[3] \qquad $a^{(1)} := x^{(t)}$
\item[4] \qquad \textbf{Forward propagation.} Compute $a^{(l)}$ for $l=2,3,...,L$. See in file "Neural networks I"
\item[5] \qquad Using $y^{(t)}$, compute: $\delta^{(L)} = a^{(L)} - y^{(t)}$. Where $a^{(L)}$ is vector of outputs from last
		  layer in one-hot-encoding form for $t-th$ example. Also, $y^{(t)}$ is one-hot-encoding vector for $t-th$ example.
\item[6] \qquad Calculate $\delta^{(L-1)},...,\delta^{(2)}$ for every layer before using: $ \delta^{(l)}=((\Theta^{(l)})^T \delta^{(l+1)}) .* a^{(l)} .*(1-a^{(l)})$. $\delta$ values are errors for every layer. If we use different activation function then delta is calculated differently. Also,
delta values in hidden layers will contain bias errors. Exclude those errors from delta vector!
\item[7] \qquad Calculation of partial derivation for cost function without regularization. $\Delta_{i,j}^{(l)} := \Delta_{i,j}^{(l)} + 						a_j^{(l)} \delta_i^{(l+1)}$. Vectorised form: $\Delta^{(l)} := \Delta^{(l)}
				+ \delta^{(l+1)} (a^{(l)})^T$
\item[8] Regularization step. $D_{i,j}^{(l)} := \frac{1}{m}(\Delta_{i,j}^{(l)} + \lambda \Theta_{i,j}^{(l)})$ if $j \neq 0$.
				$D_{i,j}^{(l)} := \frac{1}{m}(\Delta_{i,j}^{(l)})$ if $j=0$

\end{itemize}
Values in $D$ matrix are result of accumulating partial derivations of regularised cost function. In other words:
\begin{equation}
\frac{\partial}{\partial \Theta_{i,j}^{(l)}} J(\Theta) = D_{i,j}^{(l)}
\end{equation}
Intuition behind back propagation is calculating error $\delta_j^{(l)}$ for layer $a_j^{(l)}$. Formally delta values 
are derivation of cost function:
\[
\delta_j^{(l)} = \frac{\partial}{\partial z_j^{(l)}}cost(t)
\]
And derivative is slope of tangent. More steep, more incorrect we are.\\
\\
\textbf{IMPLEMENTATION NOTE.}\\
Matrices $D$ and $\Theta$ are useful for vectorised form. Some functions for advanced minimization requires to unroll
these matrices in vectors. Also, vectors can be rolled into matrices. See code for explanation.\\
\\
\textbf{GRADIENT CHECKING}
Back propagation is one way to calculate gradient of cost function. To check if back propagation is well implemented
we can calculate gradient numerically and compare results. Numerical calculation of gradient is very slow for large number
of examples and large networks and that is why it is not used for actual calculation.\\
Idea is to create simple network and small number of examples to check back propagation algorithm with numerical calculation.\\
Gradient of cost function can be approximated by:
\begin{equation}
\frac{\partial}{\partial \Theta_j} J(\Theta) \approx \frac{J(\Theta_1,...,\Theta_j + \epsilon, ..., \Theta_n) - J(\Theta_1,...,\Theta_j - \epsilon, ..., \Theta_n)}{2 \epsilon}
\end{equation}
This is called two side differentiation which is more accurate.\\
See code for implementation.\\
\\
\textbf{RANDOM INITIALIZATION.}\\
If all weights are initial set to zero or any other same value then hidden layers compute exactly the same function of input.
All hidden units compute exact same feature - problem of symmetric ways when using back propagation.\\
That is why we use random initialization and that is how symmetry is broken. Small values near zero.\\
Random weights should be in range $[-\epsilon_{init},+\epsilon_{init}]$. Effective way of choosing $\epsilon_{init}$:
\begin{equation}
\epsilon_{init} = \frac{\sqrt{6}}{\sqrt{L_{int} + L_{out}}}
\end{equation}
Where $L_{in}=s_l$, $L_{out}=s_{l+1}$ is number of units adjacent to $\theta^{l}$.\\
\\
\textbf{Summary.}
\begin{itemize}
\item[1] \textbf{Pick network architecture}. Number of input nodes corresponds to dimension of features. Number of output units correspond
		to number of classes. For multi class we use one-hot-encoding. Default number of hidden layers is one. Number of units in 
		hidden layer is one, two, three, four times than number of input units. All hidden layers have same number of units. \\
\item[2] \textbf{Train NN}. 
\item[3] \qquad Randomly choose initial weights. 
\item[4] \qquad Implement forward propagation to get $h_{\theta}(x^{(i)})$ for every $x^{(i)}$.
\item[5] \qquad Implement cost function $J(\Theta)$. Cost function is non-convex!
\item[6] \qquad Implement back propagation to calculate gradient of cost function.
\item[7] \qquad Compare gradient from back propagation and numerically calculated gradient.\\
\item[8] \qquad Use gradient descend or advanced minimization techniques to minimise cost function. Minimal value of cost 
			function is place of best weights that fit data and hypothesis is close to $y$. Plotting cost function and
			number of iterations is good way to verify.
\end{itemize}
\end{document}
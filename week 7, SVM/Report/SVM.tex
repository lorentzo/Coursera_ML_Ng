\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\textbf{week 7.}\\

\section{Large margin classifier}

\subsection{Optimization objective}
Cost function will be introduced using analogy to logistic regression.\\
In case $y=1$ we want $h_{\theta}(x) \approx 1$ in other words: $\theta^Tx>>0$. We
can see explanation in figure \ref{im:analogyLG}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{analogyLG}
\centering
\caption{Left: logistic function. This explains $\theta^Tx>>0$. Right: logistic 
			regression cost function (red) for case $y=1$ and SVM cost function (blue)}
\label{im:analogyLG}
\end{figure}
Similar result is for case when $y=0$.\\
With analogy to logistic regression we define SVM cost function as:
\begin{equation}
J(\theta) = C \sum_{i=1}^{m}(y^{i} cost_1(\theta^T x^{(i)}) + 
							(1-y^{i}) cost_0(\theta^T x^{(i)})) +
							\frac{1}{2} \sum_{i=1}^{n} \theta_j^2
\label{eq:SVMcost}
\end{equation}
Where $cost_0$ denotes for negative examples and $cost_1$ denotes for positive 
examples.\\
Now optimization objective is:
\begin{equation}
min_{\theta} J(\theta)
\end{equation}

\subsection{Large margin intuition}
In figure \ref{im:analogyLG} SVM cost function is given on right in blue. Definition 
of cost function if $y=1$ we want $\theta^Tx \geq 1$. It is also ok if
$\theta^Tx \geq 0$\\
\\
\textbf{Decision boundary}\\
During minimization when $C$ is large then sum first sum in equation \ref{eq:SVMcost} 
must be small as possible, then minimization problem turns into:
\begin{equation}
min_{\theta} \frac{1}{2} \sum_{i=1}^{n} \theta_j^2
\label{eq:SVMminHighC}
\end{equation}
Note: $C$ value is regularization value. Connection: $C \sim \frac{1}{\lambda}$. Large value of $C$ 
is making algorithm sensitive to outliers (high variance, over fitting).\\
This is important for understanding how is algorithm finding hypothesis that is separating classes.\\

\subsection{Understanding math behind SVM}
For dataset in figure \ref{im:margin} there are many ways to separate classes. SVM is trying to 
find largest margin and it is doing so by using equation \ref{eq:SVMminHighC}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{margin}
\centering
\caption{SVM is trying to find largest margin}
\label{im:margin}
\end{figure}
Decision boundary showing in green is most reasonable. Intuition how SVM finds that boundary
is following:
\begin{itemize}
\item Main idea is to minimize equation \ref{eq:SVMminHighC} which is equal to $\frac{1}{2}||\theta||^2$.
		This is case when $C$ is large!
\item Hypothesis is $\theta^Tx^{(i)} \geq 1$ also can be written as $p^{(i)}||\theta|| \geq 1$. Where $p^{(i)}$ is
		length of projection of vector $x$ on vector $\theta$.
\item Vector $\theta$ is perpendicular to decision boundary (decision boundary is defined by 
	  $\theta$ vector).
\item In figure \ref{im:projection} we can see optimal boundary. Boundary is optimal because
		projection of examples $p^{(i)}$ are large so $||\theta||$ is small and that is what we
		wanted to achieve. Different positions of decision boundary have different position of
		theta vector and projections of examples are smaller therefore value $||\theta||$ is 
		not minimal.
\end{itemize}

\begin{figure}[H]
\includegraphics[scale=0.5]{projection}
\centering
\caption{Optimal decision boundary}
\label{im:projection}

\end{figure}

\section{Kernels}

\subsection{Kernels I}
For complex decision boundary we can use high order polynomial features. Another 
way to create features is using kernels.\\
Let's define:
\begin{equation}
h_{\theta}(f) = \theta_0 + \theta_1 f_1 + \theta_2 f_2 + ...
\end{equation}
Values $f_i$ are called kernels.\\
\\
\textbf{Polynomial kernels}.\\
For polynomial features we have following kernels:\\
$f_1 = x_1$, $f_2 = x_2$, $f_3 = x_1 x_2$, $f_4 = x_1^2$, $f_5 = x_2^2$... \\
\\
\textbf{Advanced kernels}.\\
Another approach is using similarity kernels. First step is to define landmarks $l$ - points
which we compare for similarity. Then kernels is:
\begin{equation}
f_i = similarity(x, l^{(i)})
\end{equation}
Where $x$ is one example.\\
If we use Gaussian kernel for similarity between all landmarks $l^{(i)}$ and one example
then:
\begin{equation}
f_i = similarity(x, l^{(i)}) = exp(-\frac{\sum_{j=1}^{n}(x_j - l_j^{(i)})^2}{2\sigma^2})
	= exp(-\frac{||x-l^{(i)}||^2}{2\sigma^2})
\label{eq:gaussKernel}
\end{equation}
If $x \approx l_i$ then $f_i=1$ else $f_i=0$.\\
Values $f_i$ represent new features.\\
To get better understanding of equation \ref{eq:gaussKernel} look at figure \ref{im:gausskernel}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{gaussKernel}
\centering
\caption{Gaussian kernel. Values $f_i$ are greater if example x is closer to
			landmark $l_i$ which is mean of Gaussian}
\label{im:gausskernel}
\end{figure}
Smaller $\sigma$ is making Gaussian thinner so examples need to be closer to landmark
(Gaussian mean) for values of features to be larger.\\
Larger $\sigma$ is making Gaussian more spread.\\
\\
\textbf{Example of prediction using kernels}
Once we define kernel. prediction of new example is:
\[
\theta_0 + \theta_1 f_1 + \theta_2 f_2 \geq 0
\]
\begin{figure}[H]
\includegraphics[scale=0.5]{kernelPred}
\centering
\caption{Decision boundary of hypothesis using kernel functions}
\label{im:kernelPred}
\end{figure}

\subsection{Kernels II}
Landmarks are chosen to be given examples. So if we have $m$ examples, we will
have $m$ landmarks.\\
Using this, for training example $(x^{(i)}, y^{(i)})$ we will obtain $m$ features calculating:
\begin{equation}
f_j^{(i)} = similarity(x^{(i)}, l^{(j)})
\end{equation} 
Where $j=1..m$.\\
Now we have vector of $m$ features for every vector instead of $n$ features.\\
\\
\textbf{SVM with kernels}\\
For SVM we can use kernel trick.\\
For given example $x$ calculate features $f$ which are vectors of $m+1$ elements.\\
Predict $y=1$ if $\theta^Tf \geq 0$.\\
Now analog to equation \ref{eq:SVMcost} define optimization objective:
\begin{equation}
J(\theta) = C \sum_{i=1}^{m}(y^{i} cost_1(\theta^T f^{(i)}) + 
							(1-y^{i}) cost_0(\theta^T f^{(i)})) +
							\frac{1}{2} \sum_{i=1}^{m} \theta_j^2
\end{equation}
Second sum now goes from $1$ till $m$ because we have $m$ features. Also, sum can be
written as $\theta^T\theta$. Additionally, we can write $\theta^TM\theta$, where $M$
is kernel matrix.\\
\\
\textbf{SVM parameters}\\
\begin{itemize}
\item High value $C = \frac{1}{\lambda}$ lower bias and higher variance.
\item Low value $C = \frac{1}{\lambda}$ lower variance and higher bias.
\end{itemize}
If we choose Gaussian kernel then:
\begin{itemize}
\item larger $\sigma$ results in smoother features variation (longer Gaussian tails) which
	results in higher bias and lower variance
\item Smaller $\sigma$ results in higher variance and lower bias
\end{itemize}
$C$ and $sigma$ values are found using grid search. That means:
\begin{itemize}
\item Create list of different $C$ and $sigma$ (e.g. 0:01; 0:03; 0:1; 0:3; 1; 3; 10; 30).
\item for every $C$ choose different $sigma$ train model on train set. Result is $|C| * |sigma|$
		hypothesis.
\item Choose hypothesis that preforms best on validation set (smallest error: fraction of 
		validation examples that are classified)
\end{itemize}

\section{using SVM}
Advice is to use software packages for optimization and finding parameters $\theta$. Examples
of packages: liblinear, libsvm...\\
But, hyper parameters as $C$ and kernel parameters need to be chosen manually using validation
set.\\
\\
\textbf{Types of kernel.}\\
\begin{itemize}
\item \textbf{No kernel}. This is linear kernel $f=x$. Standard linear classifier. Good 
		if number of features is large and number of examples small.
\item \textbf{Gaussian kernel}. As in equation \ref{eq:gaussKernel}. And $l$ are given
		examples $x$. Need to specify $\sigma$. use if number of features small and
		number of examples large. Use normalization!\\
\item Other: polynomial kernel ($K(x,l) = x^Tl + c)^d$), string kernel, chi-squared kernel,
 histogram section kernel...
\end{itemize}
\textbf{Multiclass classification}\\
Use one-VS-all method. Train $K$ SVMs and obtain $K$ parameters $\theta^{(i)}$. Pick class $i$
which has the highest $\theta^{(i)^Tx}$
\\
\textbf{Logistic regression VS SVM}
Choice depends on number of features $n$ relative to number of examples $m$.\\
\begin{itemize}
\item If $n$ large relative to $m$ ($n=10000$, $m=10..1000$) then use logistic regression
or SVM with linear kernel.
\item If $n$ small and $m$ intermediate ($n=1..1000$, $m=10..10000$) use SVM with 
	Gaussian kernel.
\item $n$ small and $m$ is large ($n=1..1000$, $m=50000+$) then create more features
	and use logistic regression or SVM with linear kernel.
\end{itemize}
\textbf{Note:} Logistic regression gives similar results to SVM with linear kernel\\
\textbf{Note:} SVM has convex optimization objective and optimization is quick while 
				NN have longer learning process and non-convex optimization.\\
\textbf{Note:} Logistic regression with kernel is slower then SVM with kernel.\\

\textbf{CS229 Approach.}
\section{Margins - intuition}
\subsection{Analogy to logistic regression}
In logistic regression we have probability $p(y|x) = h_{\theta}(x) = \sigma(\theta^Tx)$. So we predict:
\begin{itemize}
\item "1" if $\theta^T x \leq 0$
\item "0" $\theta^T x < 0$
\end{itemize}
As we use sigmoid function, in other words we are more confident in prediction if $\theta^T x >> 0$ or 
if $\theta^T x << 0$. Then we can say that we have found good fit $\theta$ to the data.\\
This is our goal in SVM.\\

\subsection{Intuition on decision boundary}
Similar examples that are closer to decision boundary have lower confidence of prediction.\\
TODO - slika\\

\section{Notation}
We will define SVM as linear classifier for binary classification problems as follows:
\begin{equation}
h_{w,b}(x) = g(w^Tx + b)
\end{equation}
Where:
\begin{itemize}
\item $b$ is intercept term as $\theta_0$
\item $w$ are other parameters $\theta$
\item We will define $g(z)$ as:
\begin{equation}
\begin{split}
g(z) = 1 \quad if \quad z \leq 0 \\
g(z) = 0 \quad otherwise
\end{split}
\end{equation}
\end{itemize}
\textbf{NB:} Classifier will return $-1$ or $1$ directly without probabilistic interpretation.\\

\section{Functional and geometrical margin}
\subsection{Functional margin}
We define functional margin $\gamma^{(i)}$ with respect to training example $(x^{(i}), y^{(i)})$ as:
\begin{equation}
\hat{\gamma^{(i)}} = y^{(i)}(w^T x^{(i)} + b)
\end{equation}
Where,
\begin{itemize}
\item $y^{(i)}$ is label of example $(x^{(i}), y^{(i)})$
\item $(w^T x^{(i)} + b)$ is distance of example $(x^{(i}), y^{(i)})$ from decision boundary $w^T x + b = 0$
\end{itemize}
So, as $\gamma^{(i)}$ is distance is should be positive and large (for confident prediction):
\begin{itemize}
\item if $y^{(i)}=1$ then for confident prediction (large margin) we need $(w^T x^{(i)} + b)$ to be large and
		positive
\item if $y^{(i)}=-1$ then for confident prediction (large margin) we need $(w^T x^{(i)} + b)$ to be large and
		negative
\end{itemize}
That results in: if $y^{(i)}(w^T x^{(i)} + b)>0$ then prediction is correct.\\
\textbf{NB:} scaling parameters $w,b$ is not changing $h_{w,b}(x)$ so we will tend to normalize this values
			as $\frac{b}{||w||}$ and $\frac{w}{||w||}$
Given train set $S=(x^{(i)},y^{(i)}), i=1...m$ we define functional margin as smallest functional margin
of given examples S:
\begin{equation}
\hat{\gamma} = min_{i=1...m}\hat{\gamma^{(i)}}
\end{equation}

\subsection{Geometrical margin}
Now we will make same inference but using geometrical approach. Idea is in figure:
\begin{figure}[H]
\includegraphics[scale=0.5]{geomarg}
\centering
\caption{Idea of margin. $\gamma^{(i)}$ represents distance from decision boundary and example $(x^{(i)},y^{(i)}$}
\label{im:geomarg}
\end{figure}
We will denote $\gamma^{(i)}$ as distance from decision boundary $w^Tx+b=0$ to example $(x^{(i)},y^{(i)}$. That is:
\[
\gamma^{(i)} = w^T x^{(i)} + b
\]
Point $B$ that lies on decision boundary is given as subtraction of vector $x^({i)}$ (which is distance from origin to
point $x^{(i)}$ and distance from decision boundary $\gamma^{(i)}$ multiplied by norm vector $w$:
\[
x^{(i)} - \gamma^{(i)} \frac{w}{||w||}
\]
As $B$ is point on decision boundary $w^Tx+b=0$ then:
\[
w^T(x^{(i)} - \gamma^{(i)} \frac{w}{||w||}) + b = 0
\]
Solving for $\gamma^{(i)}$ gives:
\[
\gamma^{(i)} = \frac{w^Tx^{(i)}+b}{||w||}
\]
This proof was for positive example. For margin $(w,b)$ with respect to any example $(x^{(i)}, y^{(i)}$ is:
\begin{equation}
\gamma^{(i)} = y^{(i)} \frac{w^Tx^{(i)}+b}{||w||}
\end{equation}
\textbf{NB:} Normalization of parameters $(w,b)$ is not changing anything. But if we normalize parameters
so to $||w||=1$ then geometric margin is equal to functional margin.\\
\textbf{NB:} Geometric margin $\gamma^{(i)}$ is invariant to rescaling the parameters $(w,b)$\\
Given train set $S=(x^{(i)},y^{(i)}), i=1...m$ we define geometrical margin as smallest geometrical margin
of given examples S:
\begin{equation}
{\gamma} = min_{i=1...m}{\gamma^{(i)}}
\end{equation}

\section{Optimization problem}
We want to maximize geometrical margin in a way that for every example we have at least functional margin $\gamma$:
\[
\begin{split}
max_{\gamma, w, b} \gamma \\ 
w.r.t \quad y^{(i)} (w^Tx^{(i)}+b) >= \gamma, \quad i=1...m \\
and ||w|| =1
\end{split}
\]
So, first is optimization objective (find maximal margin). Second is constraint that every example must have at 
least distance $\gamma$. Third is constraint that is making functional and geometrical margin are the same.\\
Problematic is constraint $||w||=1$ which is making this problem non convex.\\
Now we are transforming this problem in:
\[
\begin{split}
max_{\gamma, w, b} \frac{\hat{\gamma}}{||w||} \\
w.r.t. \quad y^{(i)} (w^Tx^{(i)}+b) >= \hat{\gamma}, \quad i=1...m
\end{split}
\]
Now we are maximizing $\frac{\hat{\gamma}}{||w||}$ with constraint that functional margin for every example
is at least $\hat{\gamma}$. Also $\gamma = \frac{\hat{\gamma}}{||w||}$ which gives that functional and
geometrical margin are equal.\\
Problem is non convex optimization goal $\frac{\hat{\gamma}}{||w||}$. So, we will make additional constraint
$\hat{\gamma} = 1$. This is scaling constraint and it is valid because scaling $(w,b)$ is not changing anything.\\
With this addition we are minimizing:
\[
\begin{split}
min_{\gamma, w,b,} = \frac{1}{2}||w||^2 \\
w.r.t. \quad y^{(i)} (w^Tx^{(i)}+b) >= 1, \quad i=1...m
\end{split}
\]
Now optimization problem is convex quadratic with linear constraints. Solution to this optimization problem gives
optimal margin classifier.\\

\section{Lagrange duality}
To solve this optimization problem we will introduce optimization problem that is dual to given one. This
dual optimization problem will:
\begin{itemize}
\item allow to use kernels that will get optimal margin classifier for high dimensional spaces
\item give us efficient algorithm for solving given quadratic problem with linear constraints. This 
		algorithm is more efficient than default quadratic problem solver algorithms.
\end{itemize}
First we will give General Lagrange constraint optimization problem that includes equality and inequality 
constraints. Then we will formulate dual problem which we will apply to margin classifier.\\
First optimization problem is called \textbf{primal:}
\begin{equation}
\begin{split}
min_w f(w) \\
w.r.t. \quad g_i(w) \leq 0, \quad i = 1,...,k \\
\qquad h_i(w) = 0, \quad i = 1,...,l
\end{split}
\end{equation}
To solve this problem we formulate \textbf{generalized Lagrangian}:
\begin{equation}
L(w, \alpha, \beta) = f(w) + \sum_i^k \alpha_i g_i(w) + \sum_i^l \beta_i h_i(w)
\end{equation}
So we write primal as:
\begin{equation}
\theta_P(w) = max_{\alpha, \beta} L(w, \alpha, \beta)
\end{equation}
For given $w$. If $w$ violates any constraints $g_i(x)$ or $h_i(x)$ then $\theta_P(w) = \inf$.\\
If $w$ satisfies constrains then $\theta_P(w) = f(w)$.\\
So, we are minimizing $f(w)$ using $\theta_P(w)$:
\begin{equation}
min_w \theta_P(w) = min_w max_{\alpha, \beta} L(\alpha, \beta, w)
\end{equation}
Solution $p* = min_w \theta_P(w)$ is called value of primal problem.\\
Now we will consider its \textbf{dual form}. We define:
\begin{equation}
\theta_D(\alpha, \beta) = min_w L(w, \alpha, \beta)
\end{equation}
To solve we need to maximize over $\alpha, \beta$:
\begin{equation}
max_{\alpha, \beta} \theta_D(\alpha, \beta) = max_{\alpha, \beta} min_w L(w, \alpha, \beta)
\end{equation}
Solution is called solution of dual problem:
\begin{equation}
d* = max_{\alpha, \beta} \theta_D(\alpha, \beta)
\end{equation}
So difference between primal and dual problem is order of applying minimization and maximization.
Also, it is valid:
\begin{equation}
d* \leq p*
\end{equation}
Under some conditions we have:
\begin{equation}
d* = p*
\end{equation}
We assume that $g_i(w)$ is convex (hessian $H > 0$), $h_i(w)$ affine ($w^T x + b$), constraints feasible
for $g_i(w)$ (exits $w$ so $g_i(w) < 0$). This assumptions allow existence of $w*$ which is solution to 
primal problem, and existence of $(\alpha*, \beta*)$ which are solutions to dual problem.\\
If $(\alpha*, \beta*)$ and $w*$ satisfies Karush-Kuhn-Tucker constraints then solution of primal is equal
to solution of dual problem.
\begin{itemize}
\item $\frac{\partial}{\partial w_i}L(w*, \alpha*, \beta*) = 0$ for $i=1..n$
\item $\frac{\partial}{\partial \beta_i}L(w*, \alpha*, \beta*) = 0$ for $i=1..l$
\item $\alpha_i g_i(w*) = 0$, for $i=1..k$. It is called \textbf{dual complementary condition}. We'll use this
 		to prove that SVM has small number of support vectors.
\item $g_i(w*) \leq 0$, for $i=1..k$
\item $alpha_i* >= 0$, for $i=1..k$
\end{itemize}
\textbf{NB:} note the dual complementary condition! Important for support vector examples later.

\section{Using Lagrange optimization in optimal margin classifier}
Optimization problem for optimal margin classifier is given as:
\[
\begin{split}
min_{\gamma, w,b,} = \frac{1}{2}||w||^2 \\
w.r.t. \quad y^{(i)} (w^Tx^{(i)}+b) >= 1, \quad i=1...m
\end{split}
\]
And this problem we will call \textbf{primal problem}
Constraint in suitable form:
\[
g_i(w) = -y^{(i)} (w^Tx^{(i)}+b) - 1 \leq 0
\]
This is constraint for every example $(x^{(i)}, y^{(i)})$.
Now we will use \textbf{KKT dual complementary condition} to explain support vectors. Dual 
complementary condition for optimal $w*$ is given as:
\[
\alpha_i g_i(w*) = 0
\]
That means if $\alpha_i > 0$ then $g_i(w) = 0$. This is for examples $(x^{(i)}, y^{(i)})$ that have
functional margin $w^Tx^{(i)} + b = 1$, then: $g_i(w) = - y^{(i)} (w^Tx^{(i)} + b) + 1 = 0$.
Depicted in figure:\\
TODO  \\
\textbf{Lagrangian} for this optimization objective:
\begin{equation}
L(w,b,\alpha) = \frac{1}{2}||w||^2 - \sum_i^m \alpha_i (y^{(i)} (w^tx^{(i)} + b) -1)
\label{eq:lagrangian}
\end{equation}
\textbf{NB:} $\beta_i$ is not needed because we don't have equality constraint.\\
\textbf{Dual problem}. First we minimize $L(w,b,\alpha)$ with respect to $(w,b)$ for fixed
$\alpha_i$. Minimization with respect to $w$:
\begin{equation}
\begin{split}
\nabla_w L(w,b,\alpha) = w - \sum_i^m \alpha_i x^{(i)} y^{(i)} = 0\\
so \\
w = \sum_i^m \alpha_i x^{(i)} y^{(i)}
\end{split}
\label{eq:minW}
\end{equation}
Minimization with respect to $b$:
\begin{equation}
\frac{\partial}{\partial b}L(w,b,\alpha) = \sum_i^m \alpha_i y^{(i)} = 0
\label{eq:minB}
\end{equation}
Plug \ref{eq:minW} in \ref{eq:lagrangian} and we get:
\begin{equation}
L(w,b,\alpha) = \sum_i^m \alpha_i - \frac{1}{2} \sum_{i,j}y^{(i)}y^{(j)}\alpha_i\alpha_j (x^{(i)})^T x^{(j)} - b\sum_i^m\alpha_i y^{(i)}
\end{equation}
Because of \ref{eq:minB} we get:
\begin{equation}
L(w,b,\alpha) = \sum_i^m \alpha_i - \frac{1}{2} \sum_{i,j}y^{(i)}y^{(j)}\alpha_i\alpha_j (x^{(i)})^T x^{(j)}
\end{equation}
Continuing our dual problem, now we must maximize this equation with respect to $\alpha$:
\begin{equation}
\begin{split}
max_{\alpha} W(\alpha) = \sum_i^m \alpha_i - \frac{1}{2} \sum_{i,j}y^{(i)}y^{(j)}\alpha_i\alpha_j <x^{(i)},x^{(j)}> \\
w.r.t \quad \alpha_i >= 0, \quad i=1,...,m \\
\quad \sum_i^m \alpha_i y^{(i)} = 0
\end{split}
\label{eq:dual}
\end{equation}
To solve this dual problem we need some algorithm. But, assume we have found optimal $\alpha*$. using this 
we can calculate $w*$ using equation \ref{eq:minW} (primal solution). Using $w*$ we can calculate $b*$ which
is average of two closest (one positive and one negative example) from margin. Equation:\\
TODO\\
\textbf{Prediction of new example}\\
When we have fitted parameters $(w,b)$ we can predict new example $x$ using $w^Tx + b$. But if we use
equation \ref{eq:minW} we can write:
\[
\begin{split}
w^Tx + b = \\
w^T (\sum_i^m \alpha_i x^{(i)} y^{(i)}) + b = \\
\sum_i^m \alpha_i y^{(i)} <x^{(i)}, x> + b 
\end{split}
\label{eq:pred}
\]
As we know: $\alpha_i = 0$ for any non-support example. So we calculate $<x^{(i)}, x>$ only for
support example where $\alpha_i \neq 0$. Also, expression $<x^{(i)}, x>$ can be used for
\textbf{Kernel trick}.\\

\section{Kernels}
In linear or logistic regression we could, using original feature $x$ of problem (attributes), map to
input features for algorithm such as $x, x^2, x^3$. So we can write this mapping (function) as:
\[
\phi(x) = [x, x^2, x^3]^T
\]
We can do the same in SVM. So, we need to replace $x$ with $\phi(x)$ which preform mapping to some
high dimension.\\
In dual problem \ref{eq:dual} and in prediction \ref{eq:pred} we have written inner product $<x,z>$. 
So we need to replace $<x,z>$ with $<\phi(x), \phi(z)>$. \\
Problem with calculating $\phi$ is expensive calculation (because of high dimension vector. So, 
we define kernel $K$ with respect to mapping $\phi$:
\[
K(x,z) = \phi(x)^T \phi(z)
\]
Now, we replace $<\phi(x), \phi(z)>$ with $K(x,z)$. Idea is that $K(x,z)$ is easier to calculate even if
mapping $\phi$ is hard to calculate. Therefore in out algorithm we will use $K(x,z)$ which is efficient
to calculate and we can get SVM to learn high dimensional feature space defined by $\phi$ - without 
explicitly finding or representing vectors $\phi(x)$.\\
\textbf{Example of polynomial kernel}\\
So, if we want mapping of input attributes (problem features) to polynomial features described by $\phi(x)$ and
$\phi(z)$ and then preforming inner product $\phi(x)^T \phi(y)$ we can define polynomial kernel $K(x,z)$ which will
have the same effect as $\phi(x)^T \phi(y)$ in a way:
\begin{equation}
K(x,z) = (x^T z + c)^d
\end{equation}
Working in this $O(n^d)$ feature space requires only $O(n)$ time to calculate with no need of explicit representation
of $\phi(x)$ and $\phi(z)$.\\
\textbf{Example of Gaussian kernel}
If $\phi(x)$ and $\phi(z)$ are close then we want $K(x,z) = \phi(x)^T \phi(z)$ to be large. Also, if $\phi(x)$ and $\phi(z)$ are
far (nearly orthogonal) then we want $K(x,z) = \phi(x)^T \phi(z)$ to be small. In this way we want $K(x,z)$ to represent
measure of similarity. So, we can use:
\begin{equation}
K(x,z) = exp(-\frac{||x-z||^2}{2\sigma^2})
\end{equation}
Value of K(x,z) is 0 if x and z are far and 1 if x and z are close.\\
\textbf{NB:} this kernel corresponds to infinite feature mapping\\
\\
\textbf{Mercer Theorem}\\
How can we, given function $K(x,z)$, tell if it is a valid kernel: if it corresponds to some feature mapping $\phi$ so that
$K(x,z) = \phi(x)^T \phi(z)$?\\
We prove that by doing following:
\begin{itemize}
\item Consider set of m points (e.g. training set $(x{(i)}, y^{(i)}$, $i=1..m$)
\item Create $m \times m$ kernel matrix $K$ so that every element is: $K_{i,j} = K(x{(i)}, x^{(j)})$ (note that
		$K$ is matrix and $K(x,z)$ is function.
\item We want $K_{i,j} = K(x{(i)}, x^{(j)}) = \phi(x{(i)})^T \phi(x{(j)})$. So, for kernel function $K(x,z)$ to be 
		valid (correspond to some feature mapping) then its kernel matrix $K$ must be symmetric positive semi definite.
		This is sufficient and necessary condition. Also with this condition kernel matrix $K$ is called Mercer matrix.
\end{itemize}
\textbf{NOTE:} Kernel method can be applied to any learning algorithm which problem features can be written in
				form of inner product $<x,z>$. Then we just replace $<x,z>$ with $K(x,z)$.
\textbf{Intuition of Kernel.} What kernel do is mapping from original feature dimensional space to high feature
		dimension space. Of course the same is doing function $\phi$ Result is that data may become linearly separable 
		(remember till now we assumed linearly separable data, this formulation of SVM is not working for linearly inseparable data\\
		
\section{Soft margin classifier - non separable case and regularization term}
To allow our linear classifier to separate non separably data in original feature dimension space we introduced mapping 
$\phi$ which is replaced by more efficient method - kernel method.\\
Nevertheless data may still be linearly inseparable. Also, data can be or become linearly separable but we can have 
a problem of outliers. To avoid overfitting to outliers or allow separating linearly inseparable data we will introduce
different formulation of margin - \textbf{soft margin}.\\
So, reformulated optimization objective:
\begin{equation}
\begin{split}
min_{w,b, \gamma} \frac{1}{2}||w||^2 + C \sum_i^m \epsilon_i \\
w.r.t \quad y^{(i)} (w^T x^{(i)} +b ) >= 1 - \epsilon_i, \quad i=1..m \\
\quad \epsilon_i >= 0, \quad i =1..m
\end{split}
\end{equation}
Now, examples will have functional margin less then 1. So for example $(x^{(i)}, y^{(i)})$ that have functional
margin $1-\epsilon_i$ we penalize optimization objective by $+ C \epsilon_i$.\\
Parameter $C$ is controlling tradeoff between:
\begin{itemize}
\item function margin to be at least one for every example (original idea of margin classifier)
\item examples having smaller margin (idea of soft margin classifier)
\end{itemize}
For this primal optimization objective we construct generalized Lagrangian:
\begin{equation}
\begin{split}
L(w,b,\alpha, \epsilon, r) = \frac{1}{2}||w||^2 + C \sum_i^m \epsilon_i  \\
							- \sum_i^m \alpha_i (y^{(i)} (w^tx^{(i)} + b) -1) \\
							- \sum_i^m r_i \epsilon_i
\end{split}
\end{equation}
Here $\alpha_i$ and $\epsilon_i$ are Lagrangian multipliers for inequality constraints.\\
First step is minimization w.r.t $w$ and $r$ (derivation of $L$ w.r.t $w,b$ setting to 0). Result of minimizing
the primal gives same $w$ and $b$:
\[
\begin{split}
w = \sum_i^m \alpha_i x^{(i)} y^{(i)} \\
\sum_i^m \alpha_i y^{(i)} = 0
\end{split}
\]
And the dual optimization objective has the same form but different constraint:
\[
\begin{split}
max_{\alpha} W(\alpha) = \sum_i^m \alpha_i - \frac{1}{2} \sum_{i,j}y^{(i)}y^{(j)}\alpha_i\alpha_j <x^{(i)},x^{(j)}> \\
w.r.t \quad 0 \leq \alpha_i \leq C, \quad i=1,...,m \\
\quad \sum_i^m \alpha_i y^{(i)} = 0
\end{split}
\]
\textbf{NB:} only different constraint is $0 \leq \alpha_i \leq C$.\\
After solving this dual problem we find $\alpha*$ with this we can calculate $w*$ and finally $b*$. But $b*$ has different
form. In fact it is only thing that changes ($w$ is the same!)\\
Now, dual-complemetary conditions from KKT:
\begin{itemize}
\item $\alpha_i=0$ $\rightarrow$ $y^{(i)} (w^T x^{(i)} +b ) >= 1$
\item $\alpha_i=C$ $\rightarrow$ $y^{(i)} (w^T x^{(i)} +b ) <= 1$
\item $0<\alpha_i<C$ $\rightarrow$ $y^{(i)} (w^T x^{(i)} +b ) = 1$
\end{itemize}

\section{SMO algorithm}
To use this algorithm we need to find how to solve dual optimization problem:
\[
\begin{split}
max_{\alpha} W(\alpha) = \sum_i^m \alpha_i - \frac{1}{2} \sum_{i,j}y^{(i)}y^{(j)}\alpha_i\alpha_j <x^{(i)},x^{(j)}> \\
w.r.t \quad \alpha_i >= 0, \quad i=1,...,m \\
\quad \sum_i^m \alpha_i y^{(i)} = 0
\end{split}
\]
SMO algorithm is used to solve this optimization problem. First we'll motivate SMO by introducing coordinate
ascend algorithm.\\

\subsection{Coordinate ascend}
Let's assume that we want to minimize function $w(\alpha_1,...,\alpha_m)$ without any constraints.\\
The idea of coordinate ascend is:
\begin{itemize}
\item[1] Until convergence:
\item[2] \qquad for i = 1,..,m:
\item[3] \qquad\qquad optimize $w(\alpha_1,...,\alpha_m)$ w.r.t $\alpha_i$ and all other $\alpha's$ fixed
\end{itemize}
\textbf{NB:} note that some heuristic can be employed to find which $\alpha$ to select for optimization.
\textbf{NB:} coordinate ascend in every iteration moves in direction parallel to coordinate axis.

\subsection{SMO}
We can't use coordinate ascend because of constraint $\sum_i^m \alpha_i y^{(i)} = 0$. Because if we optimise only
one $\alpha_i$ in every iteration then for example $\alpha_1 y^{(1)} = - \sum_{i=2}^m \alpha_i y^{(i)}$ so it
needs to be constant.\\
So, idea is to choose as few as possible optimisation variables $\alpha$ so we can minimise this problem. In this
case it is enough to take two different $\alpha_i$ and $\alpha_j$, and do the following:
\begin{itemize}
\item[1] Using some heuristic (John Platt: SMO) pick $\alpha_i$ and $\alpha_j$
\item[2] Optimize $W(\alpha)$ using $\alpha_i$ and $\alpha_j$ and keeping other $\alpha$ fixed
\item[3] Repeat until convergence (check using KKT conditions)
\end{itemize}
This minimization is efficient because it turns out as finding minimum of quadratic function on line 
inside box constraints. - TODO: explain in detail\\

\end{document}




























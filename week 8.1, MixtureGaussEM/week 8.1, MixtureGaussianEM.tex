\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\section{Mixture of Gaussian}
Similarly as k means we are in unsupervised setting and we are given data points $\{(x^{(i)})\}$, $i=1,..,.$ without
class labels.\\
Ideas is that we want to build a model from data. Then for assessing new example we use that model for prediction
of class for new example. Also, this model can be used for anomaly detection.\\
We want an algorithm that will given the data find Gaussian distributions that generated that data. As we have 
more then one cluster then more then one Gaussian is needed to be estimated.\\
Example:
\begin{figure}[H]
\includegraphics[scale=0.5]{gaussmix}
\centering
\caption{One dimensional dataset with two hidden labels (two clusters). We estimate Gaussian distribution for 
		every cluster}
\label{im:mixgauss}
\end{figure}
So, we will introduce variable $z$ which is latent (hidden/unknown). And we will model data with joint distribution:
\begin{equation}
p(x^{(i)}, z^{(i)}) = p(x^{(i)}|z^{(i)})p(z^{(i)})
\end{equation}
Where $z^{(i)}$ is Multinomial($\phi$) if we have more then one cluster (more than one Gaussian)
or Bernoulli($\phi$) if we have two clusters.\\
And $x^{(i)}|z^{(i)=j}$ is Gaussian($\mu_j$, $\sigma_j$). Where we have separate $\mu_j$ and $\sigma_j$
for every cluster (latent class).\\
This joint distribution can be described as first choosing value of $z^{(i)}$ from multinomial (or Bernoulli) 
distribution and then randomly choosing $x^{(i)}$ from Gaussian distribution distribution. So this is how
new example $(x^{(i)}, z^{(i)})$ is generated.\\
\\
\textbf{Connection to Gaussian discriminant analysis}. Difference between mixture of Gaussian and Gaussian
discriminant analysis is that target labels are known. So in GDA we use MLE for estimating parameters
of Gaussian distribution specifically for every class (because we know class labels of every example). But
in the end for both approaches we build a model for data.\\
\\
In case of mixture of Gaussian target variables $z^{(i)}$ are unknown so we need to employ bootstrap method
to "guess" target values and then update model parameters in every iteration. We use this method because it is impossible to solve this
problem of estimation by writing log likelihood and using MLE as we did in GDA. If latent variables would be known
we would have very similar estimation process as in GDA (with difference that in Gaussian mixtures we use multinomial
distribution and we use separate covariance matrix for every Gaussian).\\
This bootstrap algorithms is called EM algorithm. It consists of two steps that repeat until convergence:
\begin{itemize}
\item[1] \textbf{E step}. In this step algorithm is trying to guess target value of latent variable $z^{(i)}$
		by using estimated (or initial) parameters of distributions that we use to model the data clusters.
		This guess is performed as:
		\begin{equation}
		w^{(i)}_j = p(z^{(i)}=1|x^{(i)};\phi,\mu,\Sigma) = \frac{p(x^{(i)}|z^{(i)}=1)p(z^{(i)}=j)}{\sum_lp(x^{(i)}|z^{(i)}=l)p(z^{(i)}=l)}
		\end{equation}
		So, for every example $i=1...m$ we find probability for every class $j=1...k$. This is posterior probability using Bayes rule.\\
		$p(x^{(i)}|z^{(i)}=1)$ is modelled with Gaussian. $p(z^{(i)}=j)$ is modelled with multinomial.\\
		$w^{(i)}_j$ is called soft guess because instead of class values (e.g. 0,1) are given probabilities in range [0,1].\\
\item[2] \textbf{M step}. We use guessed target values for every example to update model parameters:
		\begin{equation}
		\begin{split}
		\phi_j = \frac{1}{m} \sum_i^m w_j^i \\
		\mu_j = \frac{\sum_i^m w_j^i x^{(i)}}{\sum_i^m w_j^i}\\
		\Sigma_j = \frac{\sum_i^m w_j^i (x^{(i)} - \mu_j) (x^{(i)} - \mu_j)^T}{\sum_i^m w_j^i}
		\end{split}
		\end{equation}
		This formulas are identical as GDA but instead of $1\{z^{(i)}\}$ we have $w^{(i)}_j$. Also, we use different 
		covariance matrices for every Gaussian.\\
\end{itemize}
\textbf{NOTE:} EM algorithm is similar as k means algorithm. Both have assignment (or guess) step and both have 
				update step.\\
\\
\textbf{NB:} EM has also problem with local optima. So re running the algorithm with different ini parameters is desired.\\
Now, we will describe more general EM algorithm which will result in this special case and give information about 
convergence. Also different distributions can be used for modelling.\\

\section{EM algorithm}

\subsection{Jensen's inequality}
To derive EM algorithm we need Jensen's inequality. So, let $f$ be convex ($f''(x) \geq 0$). Let $x$ be random variable. Then
$f(E(x)) \leq E(f(x))$. Shown on figure:
\begin{figure}[H]
\includegraphics[scale=0.5]{jensen}
\centering
\caption{Toy example of Jensen's inequality.}
\label{im:jensen}
\end{figure}
Also, if $f''(x) > 0$ (strongly convex) then $f(E(x)) = E(f(x))$ iff x is constant (E(x) = x).\\
Similar, if ($f''(x) \leq 0$) (concave) then $f(E(x)) \geq E(f(x))$. So, for concave we have same results but with reversed
inequality.

\subsection{EM algorithm}
We have model $p(x,z;\theta)$. We observe only $x$. So we have estimation problem of $p(x,z;\theta)$ given the training
set $\{(x^{(i)})\}$, $i=1,..,.$.\\
We can fit the parameters of model $p(x,z;\theta)$ by maximising log likelihood:
\begin{equation}
\begin{split}
l(\theta) = \sum_i^m log p(x^{(i)}; \theta) = \\
= \sum_i^m log \sum_{z^{(i)}} p(x^{(i)}, z^{(i)}; \theta) \quad marginalization \quad by \quad z^{(i)}
\end{split}
\end{equation}
In case where $z^{(i)}$ are known then maximization of log likelihood is easy. So, we need to use EM algorithm 
which will give efficient way for maximizing the likelihood.\\
So, EM algorithm has two steps:
\begin{itemize}
\item[1] \textbf{E step} which construct lower bound on likelihood $l(\theta)$.
\item[2] \textbf{M step} which is optimizing lower bound
\end{itemize}
Now we continue. For each $i$, let $Q_i$ be some distribution over $z's$. Now we divide and multiply by $Q_i(z^{(i)})$:
\begin{equation}
\begin{split}
\sum_i^m log \sum_{z^{(i)}} p(x^{(i)}, z^{(i)}; \theta) = \\
= \sum_i^m log \sum_{z^{(i)}} Q_i(z^{(i)}) \frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})} 
\end{split}
\end{equation}
If we imagine $Q_i(z^{(i)})$ is $p(z)$, $\frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})}$ is $g(z)$ then
$\sum_z p(z) g(z) = E(g(z))$. So we have:
\begin{equation}
\sum_i log E_{(z^{(i)})} (\frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})})
\end{equation}
Now, we apply Jensen inequality:
\begin{equation}
\sum_i log E_{(z^{(i)})} (\frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})}) \geq 
\sum_i E_{(z^{(i)})} log (\frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})})
\end{equation}
For any distribution $Q_i$ we have lower bound on $l(\theta)$. We'll make inequality hold with equality 
for specific $\theta$ if we choose:
\begin{equation}
Q_i(z^{(i)}) \tilde\quad p(x^{(i)}, z^{(i)};\theta)
\end{equation}
So, we choose:
\begin{equation}
Q_i(z^{(i)}) = p(z^{(i)}|x^{(i)};\theta)
\label{eq:distrQ}
\end{equation}
Which is posterior distribution of $z^{(i)}$ given $x^{(i)};\theta$.\\
Choice of equation \ref{eq:distrQ} is actually construction of lower bound on $l$ (E step) which we maximize
in M step. This is general EM algorithm:
\begin{itemize}
\item \textbf{E step:}
\begin{equation}
Q_i(z^{(i)}) := p(z^{(i)}|x^{(i)};\theta)
\end{equation}
\item \textbf{M step:}
\begin{equation}
\theta := arg max_{\theta} \sum_i^m log \sum_{z^{(i)}} Q_i(z^{(i)}) \frac{p(x^{(i)}, z^{(i)}; \theta)}{Q_i(z^{(i)})}
\end{equation}

\end{itemize}












\end{document}
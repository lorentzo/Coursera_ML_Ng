function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

n = size(X,2);

%cost function
sum = 0;
for i = 1:m
    hi = sigmoid(X(i,:) * theta);
    sum = sum - y(i) * log(hi);
    sum = sum - (1 - y(i)) * log(1 - hi);
end
sum = sum / m;

%regularization
reg = 0;
for i = 2:n %do not regularize theta0
    reg = reg + theta(i) ^ 2;
end
reg = lambda * (1 / (2*m)) * reg;

%cost
J = sum + reg;


%gradient

for j = 1:n
    
    sum = 0;
    for i = 1:m
        hi = sigmoid(X(i,:) * theta);
        sum = sum + (hi - y(i)) * X(i, j);
    end
    
    sum = sum / m;
    
    if j == 1 %not regularize theta0
        grad(j) = sum;
        
    else %regularize theta j != 0
        grad(j) = sum + (lambda / m) * theta(j);
    end

end

% =============================================================

end

\contentsline {section}{\numberline {1}Classification and representation}{2}
\contentsline {subsection}{\numberline {1.1}Logistic regression hypothesis}{2}
\contentsline {subsection}{\numberline {1.2}Interpretation of hypothesis}{3}
\contentsline {subsection}{\numberline {1.3}Decision boundary}{3}
\contentsline {section}{\numberline {2}Logistic regression model}{4}
\contentsline {subsection}{\numberline {2.1}Cost function}{5}
\contentsline {subsection}{\numberline {2.2}Gradient descend}{6}
\contentsline {subsection}{\numberline {2.3}Advanced optimization with Newton's method}{6}
\contentsline {subsection}{\numberline {2.4}Advanced optimization in practice}{7}
\contentsline {section}{\numberline {3}Digression: perceptron learning algorithm}{8}
\contentsline {section}{\numberline {4}Multiclass classification}{8}
\contentsline {section}{\numberline {5}Problem of overfitting}{9}
\contentsline {subsection}{\numberline {5.1}Regularization and cost function}{9}
\contentsline {subsubsection}{\numberline {5.1.1}Regularized linear regression}{10}
\contentsline {subsubsection}{\numberline {5.1.2}Regularized logistic regression}{11}
\contentsline {section}{\numberline {6}Generalized linear models}{11}
\contentsline {subsection}{\numberline {6.1}Exponential family}{11}
\contentsline {subsection}{\numberline {6.2}Constructing GLMs}{13}
\contentsline {subsubsection}{\numberline {6.2.1}Linear regression model}{13}
\contentsline {subsubsection}{\numberline {6.2.2}Logistic regression model}{14}
\contentsline {subsubsection}{\numberline {6.2.3}Softmax regression}{14}
\contentsline {subsection}{\numberline {6.3}Summary of GLM}{14}

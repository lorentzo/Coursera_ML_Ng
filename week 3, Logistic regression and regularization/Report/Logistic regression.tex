\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\tableofcontents
\newpage

\textbf{Week 3.}

\section{Classification and representation}
Small discrete number of labels. Binary classification $y=\{0,1\}$. Multi-class classification $y=\{0,1,3,4...\}$.\\
If we apply linear regression to this problem then we have figure \ref{im:linregclas}.\\
Idea is to threshold linear regression output: if $h_{\theta}(x) = \theta^T x \geq 0.5$ then $y=1$ and vice versa.\\
\begin{figure}[H]
  \includegraphics[scale=0.5]{linregclas}
  \centering
  \caption{Applying linear regression to classification problem. Right: correct. Left: outlier problem}
  \label{im:linregclas}
\end{figure}
Also linear regression can give values that are $<0$ or $>1$.\\ 

\subsection{Logistic regression hypothesis}
We want $0 \leq h_{\theta}(x) \leq 1$. Therefore we use \textbf{sigmoid (logistic)} function $\sigma(z)$ figure 
\ref{im:sigmoid}. Now, hypothesis is:
\begin{equation}
h_{\theta}(x) = \sigma(\theta^T x) = \frac{1}{1 + e^{-\theta^T x}}
\label{eq:logreghyp}
\end{equation}

\begin{figure}[H]
  \includegraphics[scale=0.5]{sigmoid}
  \centering
  \caption{sigmoid function}
  \label{im:sigmoid}
\end{figure}

In classification decision boundary is linear function of $x$ (even \textbf{activation function} in this case logistic
function is not linear). But these models are not linear in parameters due to non linear activation function.\\
Any function that has property of $f(x) -> 0$ when $x->-\inf$ and $f(x) -> 1$ when $x -> +\inf$ can be used as activation
function but choice of sigmoid will be discussed.\\

\subsection{Interpretation of hypothesis}
As $0 \leq h_{\theta}(x) \leq 1$, interpretation is probabilistic. We write:
\begin{equation}
p(y=1|x;\theta) + p(y=0|x;\theta) = 1
\end{equation}
So, if $p(y=1|x;\theta) = 0.96$ then probability for $x$ to be labelled as $y=1$ is $96\%$.\\

\subsection{Decision boundary}
Decision was: $y=1$ if $h_{\theta}(x) \geq 0.5$. In figure \ref{im:sigmoid} we can see if $z>0$ then $g(z) \geq 0.5$.
So, if $\theta^T x \geq 0$ then $h_{\theta}(x) \geq 0.5$ and $y=1$. Label is decided with $\theta^T x$. This is what
we wanted from linear regression.\\
So, $\theta^T x$ defines decision boundary, figure \ref{im:decbond}.\\

\begin{figure}[H]
  \includegraphics[scale=0.5]{decbound}
  \centering
  \caption{First: linear decision boundary. Second: nonlinear boundary. Third: high polynomial decision boundary}
  \label{im:decbond}
\end{figure}

In figure \ref{im:decbond} we have:
\[ h_{\theta}(x) = \sigma(\theta_0 + \theta_1 x_1 + \theta_2 x_2) \]
\[ h_{\theta}(x) = \sigma(\theta_0 + \theta_1 x_1 + \theta_2 x_2 + \theta_3 x_1^2 + \theta_4 x_2^2) \]
So we can see that different values of theta define different curves.\\
Note: Parameters $\theta$ are fitted from data. Decision boundary is property of parameters not data.\\

\section{Logistic regression model}
Define cost function which is objective for fitting parameters.\\
Given training set: $(x^{(1)}, y^{(1)}), ..., (x^{(m)}, y^{(m)})$. Where $x = [x_0,...x_n]^T$. And $y=\{0,1]\}$.\\
Hypothesis is equation \ref{eq:logreghyp}.\\
So, 
\begin{equation}
J(\theta) = \frac{1}{m} \sum_{i=0}^{m} cost(h_{\theta}(x^i), y^i)
\label{eq:gencost}
\end{equation}
Using same cost function as linear regression is not valid because of hypothesis \ref{eq:logreghyp} cost function
will not be convex.\\
\\
So, we want to find cost function for this problem. Our goal is:
\begin{equation}
\begin{split}
p(y=1|x;\theta) = h_{\theta}(x) \\
p(z=0|x;\theta) = 1 - h_{\theta}(x)
\end{split}
\end{equation}
More compactly:
\begin{equation}
p(y|x;\theta) = h_{\theta}(x)^y (1-h_{\theta}(x))^{(1-y)}
\end{equation}
Now we want to find parameters $\theta$ that best fit that data. Using \textbf{likelihood} we perceive $p(y|x;\theta)$ as
function of $\theta$. With assumption of IID:
\begin{equation}
L(\theta) = p(y|x;\theta) = \prod_i^m p(y^{(i)}|x^{(i)};\theta)
\end{equation}
For simplicity we maximize $l(\theta) = log(L(\theta))$:
\begin{equation}
\begin{split}
l(\theta) = \sum_i^m log(p(y^{(i)}|x^{(i)};\theta)) = \\
		  = \sum_i^m y^{(i)} log(h_{\theta}(x^{(i)})) + (1-y^{(i)})log(1-h_{\theta}(x))
\end{split}
\label{eq:logisticL}
\end{equation}
Maximizing this log likelihood leads to minimization of cost function:
\begin{equation}
J(\theta) = - (y^{(i)} log(h_{\theta}(x^{(i)})) + (1-y^{(i)})log(1-h_{\theta}(x)))
\end{equation}
Which will give optimal parameters $\theta$ for decision boundary.\\

\subsection{Cost function}
Therefore we define for one training example:
\begin{equation}
cost(h_{\theta}(x), y) = 
- y log(h_{\theta}(x)) 
- (1 - y) log(1-h_{\theta}(x))
\label{eq:cost}
\end{equation}
In figure \ref{im:cost} we can see cases.\\
\begin{figure}[H]
  \includegraphics[scale=0.5]{cost}
  \centering
  \caption{Cost function. Black: $y=0$. Green: $y=1$}
  \label{im:cost}
\end{figure}
So, if $y=1$ then $-log(h_{\theta}(x))$ is active and penalty is high if $h_{\theta}(x)$ approaches 0, on figure \ref{im:cost}
this is green curve, cost is low if $h_{\theta}(x)$ approaches 1. Vice versa for $y=0$.\\
We plug equation \ref{eq:cost} in equation \ref{eq:gencost}:
\begin{equation}
J(\theta) = \frac{1}{m} \sum_{i=0}^{m} [- y^{(i)} log(h_{\theta}(x^{(i)})) - (1 - y^{(i)}) log(1-h_{\theta}(x^{(i)}))]
\label{eq:logregfull}
\end{equation}
And vectorized version is:
\begin{equation}
J(\theta) = \frac{1}{m}(-y^T log(h) - (1-y)^T log(1-h))
\end{equation}
Where:
\[ h = \sigma(X \theta) \]

\subsection{Gradient descend}
Now using maximum likelihood estimator we want to maximize equation \ref{eq:logisticL}. For maximization we can use
gradient descend:
\begin{equation}
\theta := \theta + \nabla_{\theta}l(\theta)
\end{equation}
Value of $\nabla_{\theta}l(\theta)$ for $j-th$ element is:
\begin{equation}
\frac{\partial}{\partial \theta_j}l(\theta) = \sum_i^m(y^{(i)} - h_{\theta}(x^{(i)}))x_j^{(i)}
\end{equation}
So, gradient descend for logistic regression cost function is:
\begin{equation}
\theta_j := \theta_j + \alpha \sum_{i=1}^m (h_{\theta}(y^{(i)} - x^{(i)}) x_j^{(i)}
\label{eq:batch}
\end{equation}
\\
Of course, same result is gain by minimising cost function \ref{eq:gencost}.\\
Repeat:
\begin{equation}
\theta_j := \theta_j - \alpha \frac{\partial J(\theta)}{\partial \theta_j}
\end{equation}   
Where,
\begin{equation}
\frac{\partial J(\theta)}{\partial \theta_j} = \frac{1}{m} \sum_{i=1}^m (h_{\theta}(x^{(i)}-y^{(i)}) x_j^{(i)}
\end{equation}
Done simultaneously for $j=0..n$.\\
This equation reminds of batch gradient descend for linear regression but logistic and linear regression use 
different hypothesis!\\
Vectorised shape:
\begin{equation}
\theta := \theta - \alpha \frac{1}{m} X^T (\sigma(X \theta) - \vec{y})
\label{eq:batchvector}
\end{equation}
Validation of learning rate $\alpha$ is done by plotting cost function $J(\theta)$ from \ref{eq:gencost} and number 
of iteration. Cost should decrease in every iteration.\\
Feature scaling is also important for faster gradient descent.\\

\subsection{Advanced optimization with Newton's method}
To minimize cost function or to maximize likelihood we can use \textbf{batch gradient descend} in equations \ref{eq:batchvector} and
\ref{eq:batch}. Also, we can use stochastic gradient descend:
\begin{equation}
\theta_j := \theta_j - \alpha(h_{\theta}(x^{(i)}-y^{(i)})x_j^{(i)}
\end{equation}
Where we simultaneously change $\theta_j$ for $j=1..n$ for every example $i=1..m$.\\
Another optimization for logistic regression is use of Newton's method. Idea of method:
\begin{figure}[H]
\includegraphics[scale=0.5]{newton}
\centering
\caption{Problem of finding minimum using Newton's method}
\label{im:newton}
\end{figure}
So to find $\theta$ where $f(\theta)=0$ we use this method. We can write:
\begin{equation}
\begin{split}
f'(\theta^i) = \frac{f(\theta^i)}{\Delta} \\
\Delta = \frac{f(\theta^i)}{f'(\theta^i)} \\
\theta^{i+1} = \theta^i - \frac{f(\theta^i)}{f'(\theta^i)}
\end{split}
\end{equation}
This method is using linear approximation in current $\theta$.\\
Using this method to maximize likelihood we want $\theta$ where $l'(\theta)=0$, so we define:
\begin{equation}
\theta^{i+1} = \theta^i - \frac{l'(\theta^i)}{l''(\theta^i)}
\end{equation}
Generalization where $\theta$ is vector:
\begin{equation}
\theta := \theta - H^{-1} \nabla_{\theta}l(\theta)
\end{equation}
Where $\nabla_{\theta}l(\theta)$ is gradient. And $H_{i,j} = \frac{\partial^2 l(\theta)}{\partial \theta_i \partial \theta_j}.$\\
Newton's method is faster then batch gradient but slower then stochastic gradient because of hessian matrix inversion.\\
Newton's methods applied to logistic regression cost function minimization is called \textbf{Fischer scoring}.\\

\subsection{Advanced optimization in practice} \label{ch:advopt}
Instead of gradient descent algorithms:
\begin{itemize}
\item Conjugate gradient
\item BFGS
\item L-BFGS
\end{itemize}
Can be used.\\
Pros:\\
\begin{itemize}
\item no need to choose $\alpha$. Line search in every iteration is done to automatically find best $\alpha$.
\item faster
\end{itemize}
Cons: more complex.\\
Octave/Matlab can be used if we define cost function and gradients (for every $\theta$) and calling specialised
functions that preform above advanced minimization.\\
See code for explanation.\\

\section{Digression: perceptron learning algorithm}
In logistic regression we used sigmoid as activation function.\\
Also we can use \textbf{step function} as activation function. This function is defined as $g(z) = 1$ if $z\leq 0$,
otherwise $g(z) = 0$.\\
This activation unit doesn't give probabilistic output.\\
Hypothesis is the same, but with this activation unit:
\[
h_{\theta}(x) = g(\theta^T x)
\]
Now, finding optimal parameters using gradient descend is following:
\begin{equation}
\theta_j := \theta_j - \alpha(y^{(i)} - h_{\theta}(x^{(i)}))x_j^{(i)}
\end{equation}
This equation reminds of stochastic gradient descend for linear regressing. But keep in mind that different hypothesis
is used!\\

\section{Multiclass classification}
Logistic regression could be used for multiclass classification ether one-vs-one (OVO) or one-vs-all (OVA). We will 
discuss OVA.\\
Idea is to find decision boundary between $ith$ class (positive) and all others, where all other classes are considered as
one class (negative).\\
For $c$ classes it is needed to train $c$ logistic regression classifiers:
\[
h_{\theta}^{(i)}(x) = p(y=i|x;\theta)
\]
Where $i=1...c$.\\
For classifying new example $x$ use:
\[
max_i{h_{\theta}^{(i)}(x)}
\]
TODO - PICTURE OF MULTICLASS

\section{Problem of overfitting}
Some data are not linearly separable (e.g. in two dimensions). More complex hypothesis are created based upon given features. For
example, features are combined in polynomial of high degree. In that way hypothesis has higher dimensionality and
examples are linearly separable in high dimension. Plotting high dimensional hypothesis in 2D seems like non-linear
line.\\
For given data chosen hypothesis (regression or classification) can be too simple or too complex.\\
Too simple hypothesis is underfitting the data:
\begin{itemize}
\item large error (cost function) on training set and test set
\item it is to simple to "understand" (fit) data
\item highly biased
\end{itemize}

Too complex hypothesis is overfitting data:
\begin{itemize}
\item small error on training set and large error
\item hypothesis has adapted the train data and it is not generalizing well
\item highly variant because it can fit any function and there is not enough data to constrain it
on test set.
\end{itemize}
Overfitting is caused if there is a lot of features and not enough examples.\\

TODO - picture of over/under fit: regression, classification

\textbf{Methods for dealing with hight dimension data.}\\
Reducing number of features:
\begin{itemize}
\item manually
\item model selection algorithms (automatically)
\end{itemize}
Regularization:
\begin{itemize}
\item keep all features, but reduce magnitude of parameters $\theta$
\item use if all features are useful
\end{itemize}

\subsection{Regularization and cost function}
Complex model for regression and classification means it has for example high polynomial level. Idea is to
reduce parameters $\theta$ next to those high level polynomials. In that way influence of high polynomial is reduced
and model is becoming more simple:
\begin{itemize}
\item simpler hypothesis
\item less prune to overfitting
\end{itemize}
Cost function has to be modified so it penalises parameters $\theta$ more. Cost function will be minimised, so parameters
must be multiplied by high value. As we don't know which parameters needs to be smaller if we have a lot of features
regularization is done on all of them.\\
Modified cost function:
\begin{equation}
J_{\lambda}(\theta) := J(\theta) + \lambda \frac{1}{2m} \sum_{j=1}^n \theta_j^2
\end{equation}
Where $J(\theta)$ is original cost function for linear regression or logistic regression.\\
Parameter $\lambda$ is trade off between prefect hypothesis fitting and small parameters $\theta$:
\begin{itemize}
\item large $\lambda$ results in smaller parameters $\theta$ which means simple hypothesis and underfit.
\item too high $\lambda$ is forcing parameters $\theta$ to zero which is "shutting" some features influences.
\item small parameter $\lambda$ is not reducing effect of regularization
\end{itemize}
Note: parameter $\theta_0$ is not regularized, and $j=1..n$.\\

\subsubsection{Regularized linear regression}
\begin{equation}
J(\theta) = \frac{1}{2m} [\sum_{i=1}^{m}(h_{\theta}(x^i) - y^i)^2 + \lambda \sum_{j=1}^n \theta_j^2]
\label{eq:reglinreg}
\end{equation}
Minimising equation \ref{eq:reglinreg} using gradient descend:
\begin{equation}
\theta_0 := \theta_0 - \alpha [\frac{1}{m} \sum_{i=1}^m (h_{\theta}(x^{(i)}-y^{(i)}) x_0^{(i)}]
\end{equation}
\begin{equation}
\theta_j := \theta_j - \alpha [(\frac{1}{m} \sum_{i=1}^m h_{\theta}(x^{(i)}-y^{(i)}) x_j^{(i)}) + \frac{\lambda}{m} \theta_j ]
\label{eq:gradregreg}
\end{equation}
Where $j=1...n$.\\
Equation \ref{eq:gradregreg} can be rewritten:
\begin{equation}
\theta_j := \theta_j (1 - \alpha \frac{\lambda}{m}) - \alpha \frac{1}{m} \sum_{i=0}^m h_{\theta}(x^{(i)}-y^{(i)}) x_j^{(i)}
\end{equation}
Part $(1 - \alpha \frac{\lambda}{m})$ is lower than 1 (e.g. 0.99). Which means that it shrinks parameters $\theta$ in 
every gradient descend iteration.\\
Also regularization applied to normal equation:
\begin{equation}
\vec{\theta} = (X^T X + \lambda L)^{-1} X^T \vec{y}
\end{equation}
Where $X$ is matrix dimension $m \times (n+1)$, y is vector dimension $m \times 1$ and $L$ is diagonal one matrix where element 
$L[0,0] = 0$, dimension $(n+1) \times (n+1)$ \\
\textbf{Problem of non invertibility.}\\
If $m \leq n$ then $X^T X$ non invertible (singular). Calculating can give inverse with zeros or small values on
diagonal.\\
But $X^T X + \lambda L$ is always invertible.\\

\subsubsection{Regularized logistic regression}
Logistic regression is prune to overfitting if:
\begin{itemize}
\item hypothesis is highly polynomial 
\item more general if there is lot of features in hypothesis (and smaller amount of examples)
\end{itemize}

\textbf{Modified cost function.}\\
Add regularization term to equation \ref{eq:logregfull}:
\begin{equation}
J(\theta) = \frac{1}{m} \sum_{i=0}^{m} [- y^{(i)} log(h_{\theta}(x^{(i)})) - (1 - y^{(i)}) log(1-h_{\theta}(x^{(i)}))] +  
\lambda \frac{1}{2m} \sum_{j=1}^n \theta_j^2
\label{eq:logregfullreg}
\end{equation}
Now parameters $\theta_1 ... \theta_n$ are penalised. Again, $\theta_1$ is not penalised!\\
Hypothesis is simpler.\\
TODO - FIGURE OF SIMPLER DICISION BOUNDARY\\
\\
\textbf{Gradient descend}
Minimising cost function in equation \ref{eq:logregfullreg}:\\
\begin{equation}
\theta_0 := \theta_0 - \alpha \frac{1}{m} \sum_{i=1}^m (h_{\theta}(x^{(i)})-y^{(i)}) x_0^{(i)}
\end{equation}
\begin{equation}
\theta_j := \theta_j - \alpha \frac{1}{m} [ (\sum_{i=1}^m (h_{\theta}(x^{(i)})-y^{(i)}) x_j^{(i)})] + \frac{\lambda}{m}\theta_j
\end{equation}
Where $j=1..n$.\\
For validating gradient descend plot equation \ref{eq:logregfullreg} for number of iterations.\\
\\
\textbf{Advanced optimization} 
Specific for matlab/ocatve define regularized cost function and derivations. Pass this to advanced minimisers as 
in section \ref{ch:advopt}.\\
See matlab code for explanation.\\

\section{Generalized linear models}

\subsection{Exponential family}
For now we have seen models $p(y|x;\theta)$ where:
\begin{itemize}
\item \textbf{$y \in R$} modelled with Gaussian. Which resulted in least squares - logistic regression.
\item \textbf{$y \in \{0,1\}$} modelled with Bernoulli. Which resulted in logistic regression. We will also explain
	why is logistic function chosen. 
\end{itemize}
Both Gaussian and Bernoulli are special cases of \textbf{exponential family}. We describe:
\begin{itemize}
\item Bernoulli($\phi$): $p(y=1;\phi)=\phi$. This is set (class) of distributions parametrized by $\phi$.
\item Gaussian $N(\mu, \sigma^2)$. This is set of distributions parametrized by $\mu$ and $\sigma^2$.
\end{itemize}
Both can be written in form of exponential family equation:
\begin{equation}
p(y=1;\eta) = b(y) exp(\eta^T T(y) - a(\eta)
\label{eq:expfam}
\end{equation}
Where,
\begin{itemize}
\item $\eta$ is natural parameter
\item $Y(y)$ sufficient statistics (usually T(y) = y)
\end{itemize}
So, for given functions $a,b,T$ we define class of distributions (e.g. class of normal distributions)
parametrized by $\eta$.\\
\\
\textbf{Bernoulli in form of exponential family}\\
\[
\begin{split}
p(y=1; \phi) = \phi \\
p(y;\phi) = \phi^y (1-\phi)^{1-y} \\
		  = exp(log(\phi^y (1-\phi)^{1-y})) \\
		  = exp(log(\frac{\phi}{1-\phi}y+log(1-\phi)))
\end{split}
\]
Where,
\begin{itemize}
\item $\eta =  log(\frac{\phi}{1-\phi})$. Solving for $\phi$: $\phi = \frac{1}{1+e^{-\eta}}$. Which 
		reminds of logistic function.
\item $a(\eta) = -log(1-\phi)$ plugging $\phi = \frac{1}{1+e^{-\eta}}$, $a(\eta) = log(1+e^{\eta})$
\item $T(y) = y$
\item $b(y) = 1$	
\end{itemize}

\textbf{Gaussian in form of exponential family.}\\
\[
\begin{split}
N(\mu, \sigma^2), \quad \sigma^2 = 1 \\
N(\mu,1) = \frac{1}{\sqrt{2 \pi}} exp(-\frac{1}{2}(y-\mu)^2)\\
		= \frac{1}{\sqrt{2 \pi}} exp(-\frac{1}{2}y^2)exp(\mu y -\frac{1}{2} \mu^2) 
\end{split}
\]
Where,
\begin{itemize}
\item $b(y) = \frac{1}{\sqrt{2 \pi}} exp(-\frac{1}{2}y^2) $
\item $\eta = \mu$
\item $Y(y) = y$
\item $a(\eta) = -\frac{1}{2} \mu^2$
\end{itemize}
Also, other distributions as: multi variate Gaussian, multinomial, Poisson, ... are classes
of distributions in exponential family.\\

\subsection{Constructing GLMs}
Consider classification or regression model where $y$ is random variable as function of $x$. To construct GLM problem
we use assumptions for conditional distribution of $y$ given $x$:
\begin{itemize}
\item[1.] $y|x;\theta$ is distributed by exponential family class distribution parametrized with $\eta$
\item[2.] Given $x$ goal is to output expected value of $T(y)$ so $E(y|x)$. So we want hypothesis 
			$h_{\theta}(x)=E(y|x)=p(y|x;\theta)$.
\item[3.] Natural parameter $\eta$ of exponential distribution class is in linear dependence of $x$ and $\theta$.
         This assumption connects given values $x$ and $\theta$ in $y|x;\theta$ with exponential family distribution
         by which $y$ is distributed and which is parametrized with $\eta$. If $\eta$ is a vector then $\eta_i=\theta_i^Tx$
\end{itemize}
\textbf{NB:} Where assumption 3. is more of design choice.\\
This steps allow us to derive class of learning algorithms (models) which we name \textbf{GLM}. Only decision we need
to take is decision of distribution which best models values $y$.\\
When we derive model we use maximum likelihood to fit optimal parameters using data (in other words we find cost 
function and minimize it for optimal parameters).\\
We can show how to derive linear regression, logistic regression and softmax regression models using this method.\\

\subsubsection{Linear regression model}
In this problem we model value $y$ \textbf{response variable} with Gaussian distribution.\\
We proved that Gaussian distribution is case of exponential family. Also we shown that $\mu=\eta$. Now,
we follow steps for constructing GLMs:
\begin{itemize}
\item[1.] We made assumption that $y$ is distributed by $ExponentFamily(\eta)$ more specifically distributed by
Gaussian distribution which is a special case
\item[2.] We want for given $x$ to output expected value $y$. So we want model so to $h_{\theta}(x) = E(y|x)$. As
 $y$ is distributed by normal distribution then $E(y|x) = \mu$. Also we inferred that $\mu=\eta$.
\item[3.] We decided for $\eta=\theta^Tx$ so finally model is: $h_{\theta}(x) = E(y|x) = \mu = \eta = \theta^Tx$
\end{itemize}

\subsubsection{Logistic regression model}
Similarly we want to classify example $x$ in $y \in \{0,1\}$. So we model $y$ using Bernoulli distribution which
is special case of exponential family. Now we derive model using GLM construction steps:
\begin{itemize}
\item[1] We model value $y$ with exponential family distribution which is parametrized with $\eta$, special case with 
Bernoulli distribution which is parametrized with $\phi$. Connection is $\phi = \frac{1}{1+e^{-\eta}}$.
\item[2] We want to find class of given value $x$, so we want model $h_{\theta}(x) = E(y|x)$. Expected value of 
			Bernoulli distribution is $\phi$.
\item[3] Finally, we model $\eta = \theta^T x$, so $h_{\theta}(x) = E(y|x) = \phi = \frac{1}{1+e^{-\eta}} = \frac{1}{1+e^{-\theta^T x}}$
\end{itemize}
\textbf{NB:} Sigmoid function is called \textbf{response function.} Inverse is called \textbf{link function}. In case
			of linear regression response function is identity function.

\subsubsection{Softmax regression}
Softmax regression is generalization of logistic regression for multi class classification problems. We derive model
using same steps as before but we assume multinomial distribution from exponential family.\\
TODO

\subsection{Summary of GLM}
So GLM method allow us to build models which are predicting target value $y$ which is modelled with exponential family
distributions. Basic steps are:
\begin{itemize}
\item[1.] For given data $(x^{(i)}, y^{(i)}$, $i=1...m$ try to model target value $y$ as random variable
of function depending on $x$. So, model $p(y|x;\theta)$ distributed by some exponential family distribution.
\item[2.] Using assumption for constructing GLMs build a model $h_{\theta}(x) = E(y|x) = p(y|x;\theta)$
\item[3.] Looking at $p(y|x;\theta)$ as function of $\theta$ using log likelihood $l(\theta)$, maximize
		log likelihood using optimization technique as gradient descend, Newton's method or some
		advanced optimization techniques. This will result in learning algorithm for our model. Now,
		we can use data and find optimal parameters $\theta$ which will we use for prediction.
\end{itemize}


\end{document}
\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\textbf{week 8. Unsupervised learning, Clustering, K means, PCA}

\section{Clustering}

\subsection{Unsupervised learning intro}
In unsupervised problem, examples with no labels are given:
\[
\{x^{(1)},...,x^{(m)}
\]
Example of unsupervised learning is clustering.\\
Applications:
\begin{itemize}
\item Separation - finding out how many segments (clusters) in problem we have
\item Clustering in astronomy, bioinformatics
\end{itemize}

\subsection{K means}
Example of unsupervised, clustering method.\\
Algorithm:
\begin{itemize}
\item[1] Randomly initialize cluster centroids $\mu_k$, where $k=1...K$, where $K$ is number of 
			clusters. $K$ must be given before running the algorithm.
\item[2] \textbf{Assignment step.} For every example $x^{(i)}$, $i=1..m$ assign cluster index $c^{(i)}$
		 based on closest cluster centroid $\mu_k$. Formally: $c^{(i)}=min_k||x^{(i)}-\mu_k||^2$, $i = 1..m$.
\item[3] \textbf{Correction step.} Move cluster centroids to average of examples assigned to that cluster.
\item[4] Repeat steps 2 and 3 until no changes.
\end{itemize}
\textbf{Note:} K means has problem with non separating clusters. Different cluster numbers should be chosen and
evaluation of results should be done.\\

\subsection{Optimization objective}
Optimization objective is:
\begin{equation}
\begin{split}
min_{(c^{(1)},..,c^{(m)},\mu_1,..,\mu_K)} J(c^{(1)},..,c^{(m)},\mu_1,..,\mu_K) = \\
	min_{(c^{(1)},..,c^{(m)},\mu_1,..,\mu_K)} \frac{1}{m} \sum_{i=1}^{m}||x^{(i)}-\mu_{c^{(i)}}||^2
\end{split}
\label{eq:optimObj}
\end{equation}
Where $c^{(i)}$ index of cluster $(1..K)$ to which example $x^{(i)}$ has been assigned to.\\
Where $\mu_{c^{(i)}}$ is cluster centroid of example $x^{(i)}$.\\
\textbf{Note:} minimization over $c^{(1)},..,c^{(m)}$ is done in step 2.\\
\textbf{Note:} minimization over $\mu_1,..,\mu_K$ is done in step 3.\\
Cost function is called is called \textbf{distortion function}. It can be shown that k means is coordinate
descend on cost function $J$. Which means that parameter $c$ is optimized while parameter $\mu$ is fixed
and vice versa.\\
Distortion function is non convex function so coordinate descend (k means) on $J$ is not guaranteed to converge
to global optimum. So problem with local optima can be solved for running the algorithm more than once.\\

\subsection{Random initialization of cluster centroids}
Assume that K < m. Randomly pick $k$ examples and assign those examples as cluster centroids.\\
\\
\textbf{Problem of local optima.}\\
K means has problem of converging in local optima.\\
Solution is to start K means clustering multiple times, and every time with different randomly chosen
cluster centroids. Calculate cost function for every run and take the best solution.\\

\subsection{Choosing number of clusters}
\textbf{Elbow method.}\\
Idea is to plot cost function for different number of clusters K as in figure \ref{im:elbow}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{elbow}
\centering
\caption{Cost function for different number of clusters K. Optimal number of clusters is in 'elbow' area.}
\label{im:elbow}
\end{figure} 
We can see that after some value $k$ cost function is not dropping rapidly. This value should be chosen
as optimal number of clusters.\\
Problem with this approach is that elbow sometimes can't be seen easily.\\
\textbf{IDEA:} this method could be used to find optimal number of segments in problem. For example 
				this can be used for finding optimal image compression. See code for explanation.\\

\section{Dimensionality reduction}


\subsection{Motivation: Data compression and visualization}
Dimensionality reduction is done for data compression which results in lower memory requirements
and faster algorithms.\\
Also it is used for visualization dataset that has high dimensions (large number of features
$n>3$.\\
Idea is given in figure \ref{im:dimred}.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{dimred}
\centering
\caption{Reduction from 2D to 1D. Left: original dataset and optimal line for projection. 
		Right: dataset projected to 1D}
\label{im:dimred}
\end{figure}
\textbf{Note:} we can see that features are correlated that is why dimensionality reduction
				gives good results.\\
\textbf{Note:} Highly correlated features represent redundancy which is removed using 
				dimenzionality reduction.\\
\textbf{Note:} Variation is larger in one direction. 
				
\subsection{PCA}
\textbf{Problem formulation.}\\
Principal component analysis (PCA) is method for finding optimal projection vector.\\
Idea is to minimize sum of squared orthogonal distances from examples and projection plane.
As it is shown in figure \ref{im:problem}
\begin{figure}[H]
\includegraphics[scale=0.5]{problem}
\centering
\caption{PCA is minimizing projection error shown in green.}
\label{im:problem}
\end{figure}
Also, another intuition is that we want to find vector so projection on that vector has maximal variation.
As in figure \ref{im:maxvar}
\begin{figure}[H]
\includegraphics[scale=0.5]{maxvar}
\centering
\caption{Idea is to maximize variation of projected data. SO green direction is better than pink direction.}
\label{im:maxvar}
\end{figure}
Formal: To reduce from $n$ dimensions to $k$ dimensions find $k$ vectors $u^{(i)}$ that span 
linear space onto which data are projected to. We also imagine that $n$ dimensional data is generated as in
factor analysis problem: first lower dimension data is sampled, then with affine transformation it is transformed
in higher dimension space and finally noise is added to data.\\
\\
\textbf{PCA vs Factor analysis}
Factor analysis is probabilistic model and parameter estimation is based on EM algorithm.\\
PCA is more direct and it uses idea of eigen vector calculation.\\
\\
Now we will derive optimization problem:
\begin{itemize}
\item Let $||u||$ be unit length vector. Then projection of vector $x^{(i)}$ is $(x^{(i)})^T u$. As $x^{(i)}$ is
a point then $(x^{(i)})^T u$ is distance from origin.
\item To maximize variation of projected data we maximize:
\begin{equation}
max_{u:||u||=1} \frac{1}{m} \sum_i^m ((x^{(i)})^T u)^2
\end{equation}
Which is after simplification:
\begin{equation}
max_{u:||u||=1}  u^T (\frac{1}{m} \sum_i^m x^{(i)} (x^{(i)})^T) u
\end{equation}
Where,
\[
\Sigma = \frac{1}{m} \sum_i^m x^{(i)} (x^{(i)})^T
\]
is covariance matrix. And $u$ is eigen vector of covariance matrix.\\
This is a result if we use Lagrangian optimization method on this optimization problem:
\[
\begin{split}
max_{u^Tu=1} u^T \Sigma u \\
L(u, \lambda) = u^T \Sigma u - \lambda (u^Tu - 1) \\
\nabla_u L(u, \lambda) = \Sigma u - \lambda u = 0
\end{split}
\]
Where $\Sigma u - \lambda u$ exactly gives that $u$ is eigen vector of covariance matrix $\Sigma$.\\
\end{itemize}
This is proof that if we want of find 1 dimension representation of data we choose $u$ to be principal
eigen vector of covariance matrix $\Sigma$.\\
For reduction to $k<n$ dimension we use first $k$ eigen vectors $u$ (with highest eigenvalues) and perform
transform: $y^{(i)} = u_i^T x^{(i)}$.\\
\textbf{NB:} Covariance matrix $\Sigma$ is symmetric so it always has $n$ eigenvectors.
\\
\textbf{PCA vs linear regression.}\\
\begin{figure}[H]
\includegraphics[scale=0.5]{pcaVSlg}
\centering
\caption{Left: PCA is minimizing projection error shown in green.
			Right: Linear regression is minimizing different error.}
\label{im:pcaVSlg}
\end{figure}
Also, linear regression is taking in account class labels while PCA is unsupervised
method.\\

\subsection{PCA algorithm}
In first step data needs to be preprocessed:
\begin{itemize}
\item given $x^{(i)}$, $i=1,..m$ Calculate mean of features: 
\begin{equation}
\mu_j = \frac{1}{m} \sum_{i=1}^m x_j^{(i)}
\label{eq:mean}
\end{equation}
\item Calculate range $s_j$: standard deviation or difference of smallest and largest feature
\item Preform mean normalization and feature scaling:
\begin{equation}
x^{(i)}_j = \frac{x^{(i)}_j - \mu_j}{s_j}
\end{equation}
\end{itemize}
Then calculate $k$ vectors which span subspace for projection:
\begin{itemize}
\item Compute covariance matrix:
\begin{equation}
\Sigma = \frac{1}{m} \sum_{i=1}^{n} (x^{(i)}) (x^{(i)})^T
\end{equation}
Which is $[nxn]$ matrix. This matrix is symmetric positive definite.
\item Use single value decomposition to calculate eigenvectors from covariance matrix
       (also eigen vectors can be calculated directly using $det(\Sigma -\lambda I)$. 
       In matlab/octave use: $[U, S, V] = svd(\Sigma)$.
\item Eigenvectors (matrix $U$) represent principal components in directions of maximal variance.
		Matrix $U$ is $[nxn]$ matrix, where every column is principal component $u^{(i)}$ which
		is a vector of $n$ elements. Now, $k$ columns vectors are needed to find subspace for
		projection. This will result in $U_{reduced}$ which is $[nxk]$ matrix.
\end{itemize}
Finally reduced data is calculated as $Z = (U_{reduced} X^T)^T$. Matrix $Z = [mxk]$ is containing $k$
dimensional dataset.

\subsection{Applying PCA}
\textbf{Reconstruction original data from compressed data.}\\
Simply: $X_{approx} = (U_{reduced} Z^T)^T$. This results in approximation of original data based
on compressed data $Z$ and transformation matrix $U_{reduced}$.\\
\begin{figure}[H]
\includegraphics[scale=0.5]{reconstruction}
\centering
\caption{Left: original data and projection plane. Middle: dimensionality reduction.
			Right: reconstruction }
\label{im:reconstruction}
\end{figure}
We can see that projection error in reconstruction is gone, so information is lost.\\
\\
\textbf{Choosing number of principal components $k$}\\
Using fraction of:
\begin{equation}
APE = \frac{1}{m} \sum_{i=1}^m ||x^{(i)}-x_{approx}^{(i)}||^2
\end{equation}
Which represents average projection error (APE).\\
And:
\begin{equation}
TV = \frac{1}{m} \sum_{i=1}^m ||x^{(i)}||^2
\end{equation}
Which represents total variation ($TV$).\\
We can choose different values of $k$ and find for which $k$ is $\frac{APE}{TV} \leq t$, where
$t \in [0,1]$ defines loss of variance. For example $t=0.01$ means that $99\%$ variance is preserved.\\
For every value of $k$ we need to calculate $APE$. And we need to find for which value $k$
value $APE$ is minimal.\\
better approach is to use matrix $S$ (from matlab/octace svd) which is diagonal matrix $S_ii I$, where
$i=1..n$. Idea is to pick $k$ and calculate:
\begin{equation}
1 - \frac{\sum_{i=1}^k S_ii}{\sum_{i=1}^n S_ii}
\end{equation}
Which gives value $t$ and $1-t$ can say how much variance is preserved.\\
This equation can be used for finding optimal $k$ or for explaining how much variance is preserved 
with number of chosen principal components.\\

\subsection{Using PCA}
\begin{itemize}
\item Dataset $(x^{(i)}, y^{(i)})$, $i=1..m$. Split in training set, validation set and test set. Normalize 
and save normalization parameters.
\item Take $x^{(i)}$ from training set. Using this data calculate PCA transformation matrix $U_{reduced}$ and 
obtain $z^{(i)}$. Now, new dataset is: $(z^{(i)}, y^{(i)})$. using this dataset train model.
\item When using validation and test set transformed values  $z^{(i)}$ must be calculated using 
transformation matrix $U_{reduced}$ obtained on train set.
\item  Also normalization of validation and test set must be done with normalization parameters obtained 
on training set.
\end{itemize}

\textbf{Tips:}
\begin{itemize}
\item Do not use PCA for problems of overfitting and trying to reduce number of features! 
Use regularization instead.
\item Use PCA only if original features are not giving satisfying result.
\end{itemize}

\subsection{SVD in PCA}
If feature vectors are $n$ dimensional where $n$ is very high then covariance matrix is $\Sigma$ is 
also large $n \times n$. We can use SVD (singular value decomposition) to approach this problem.\\
General SVD decomposition is: 
\[
A = U D V^T
\]
Where $D$ is diagonal $n \times n$ matrix with singular values of A. And Columns of U matrix ($m \times n$) 
are eigen vectors of $AA^T$. And columns of V matrix ($n \times m$) are eigen vectors of $A^TA$.\\
The thing we know about covariance matrix $\Sigma$ is $\Sigma = X^T X$. Where $X$ is design matrix
where examples are put in rows. Then we can calculate SVD on X:
\begin{equation}
X = U D V^T
\end{equation}
Where top k columns of V are eigen values of $X^T X$ which is exactly covariance matrix $\Sigma$.\\
Important part is that $X$ is $m \times n$ matrix, where $m << n$ is cases we use SVD.\\
Also, take look at other decompositions: LU and QR (?)\\

\subsection{Applications of PCA}
\begin{itemize}
\item visualization (reduction to 2 or 3 dimensions)
\item compression
\item learning with lower dimension: higher algorithm speed or avoiding overfitting 
\item anomaly detection
\item matching/distance calculation: example in face recognition. If original image is 100x100 then
	 every face is 10000 dimensional vector. We can reduce dimension - so we acquire eigen faces.
	 That means that we only take crucial information about face without noise. And then
	 we can calculate distance/similarity between eigen faces.
\item text similarity. To describe text we can use vector features as in email problem. This 
		vectors have huge dimensionality. TO measure similarity we use cosine distance. Problem 
		with cosine distance is nominator $(x^{(i)})^T x^{(i)}$. Where this product is 0 if 
		no elements are the same. So, in high dimensional space this can be the case. If we use
		PCA we can find some (latent) similarities. This application of PCA is called LSI (latent
		semantic indexing).
\end{itemize}

\textbf{Summary of unsupervised learning}\\
We can categorize algorithms in two classes:
\begin{itemize}
\item \textbf{density estimation (model p(X)}. This is factor analysis which is similar to PCA. And
		it is used if some useful information is in subspace of data. Another is mixture of Gaussian
		which is similar to k means and it is used if we are searching for clusters in data
\item \textbf{not probabilistic}. PCA for information in subspace. K means for clustering.
\end{itemize}











\end{document}
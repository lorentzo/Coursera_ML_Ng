\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\tableofcontents
\newpage

\textbf{Generative Learning algorithms}

\section{Generative VS discriminative learning algorithms}
Informally, \textbf{discriminative learning algorithm} is trying to find discriminative boundary between classes. In other
words that mean algorithm is trying to learn only $p(y|x;\theta)$ (e.g. logistic regression). Or it learns hypothesis that
outputs class value.\\
Different approach is \textbf{generative learning algorithm}. In this approach algorithm is trying to create model for 
every class based on data. In other words it models $p(x|y)$. So, algorithm takes all positive examples and creates model
$p(x|y=1)$, then it takes all negative examples and creates a model $p(x|y=0)$. New example $x$ is matched to model and
target is given using:
\begin{equation}
p(y=1|x) = \frac{p(x|y=1)p(y)}{p(x)}
\label{eq:bayes}
\end{equation}
Where $p(x)$ is called \textbf{prior}, and $p(x|y=1)$ is called \textbf{conditional likelihood}.\\
Value $p(y=1|x)$ is called \textbf{posterior probability} and it is calculated using \textbf{Bayesian rule}.\\
Where $p(x) = p(x|y=1)p(y=1) + p(x|y=0)p(y=0)$\\
So, predicting target value $y$ of new example $x$ we use:
\begin{equation}
argmax_y\{\frac{p(x|y=1)p(y)}{p(x)}\}
\label{eq:argmax}
\end{equation}

\section{Gaussian discriminant analysis}
In this section we will model conditional likelihood using multivariate Gaussian distribution. So, 
$p(x|y)$ will be modelled using multivariate Gaussian because we assume $x$ is vector of continuous values.\\
Multivariate Gaussian takes in account dependencies between values $x$.\\

\subsection{About multivariate Gaussian}
So, we say that some random variable $Z$ is distributed by multivariate Gaussian with parameters $\vec{\mu}$ and
$\Sigma$. Distribution is written as:
\begin{equation}
p(Z) = \frac{1}{(2\pi)^{\frac{n}{2}}|\Sigma|^{\frac{1}{2}}} exp(-\frac{1}{2}(x-\mu)^T\Sigma^{-1}(x-\mu))
\end{equation}
Where,
\begin{itemize}
\item $\vec{\mu}$ is mean vector of vector data $x^{(i)}$
\item $\Sigma$ is covariance matrix where $\Sigma=E[(x-\mu)(x-\mu)^T]$. Depending on covariance matrix distribution is 
		changing in direction and size. 
\end{itemize}

\subsection{Gaussian discriminant analysis model}
So, we assume that examples $x$ are continuous therefore we model them with multivariate Gaussian. We create model
for every class. So conditional likelihood is:
\[
\begin{split}
p(x|y=1) = \frac{1}{(2\pi)^{\frac{n}{2}}|\Sigma|^{\frac{1}{2}}} exp(-\frac{1}{2}(x-\mu_1)^T\Sigma^{-1}(x-\mu_1)) \\
p(x|y=0) = \frac{1}{(2\pi)^{\frac{n}{2}}|\Sigma|^{\frac{1}{2}}} exp(-\frac{1}{2}(x-\mu_0)^T\Sigma^{-1}(x-\mu_0)) 
\end{split}
\]
Also, we assume that target value $y$ has only class $0$ and class $1$. So We model posterior probability using:
\[
p(y) = \phi^y(1-\phi)^{1-y}
\]
Using Bayesian formula \ref{eq:bayes} we can now model posterior probability $p(y|x)$ using likelihood and prior.\\
We will use maximum likelihood estimation from data to estimate parameters using \textbf{joint likelihood}
\[
\begin{split}
l(\phi, \mu_0, \mu_1, \Sigma) = log(\prod_i^m(p(y^{(i)},x^{(i)};\phi,\mu_0,\mu_1,\Sigma)) \\
= log(\prod_i^m(p(x^{(i)}|y^{(i)};\phi,\mu_0,\mu_1,\Sigma)p(y;\phi))
\end{split}
\]
\textbf{NB:} in logistic regression (as discriminant model) we were calculating $l(\theta)=log\prod_i^m p(y^{(i)}|x^{(i)};\theta)$. So, 
only and directly likelihood.\\
Now with maximum likelihood $l(\phi, \mu_0, \mu_1, \Sigma)$. We get estimation of parameters from data:
\begin{equation}
\phi = \frac{\sum_i^my^{(i)}}{m} = \frac{\sum_i^m 1{y^{(i}=1}}{m}
\end{equation}
Which is estimating Bernoulli parameter $\phi$ using target values from data. It only calculates fraction of positive examples.\\
\begin{equation}
\mu_0 = \frac{\sum_i^m1:\{y^{(i)=0}\}x^{(i)}}{\sum_i^m 1:\{y^{(i)}=0\}}
\end{equation}
Which is estimating mean parameter of multivariate Gaussian distribution from data. It only calculates mean of data vectors $x$
for which class is $y=0$.\\
\begin{equation}
\mu_1 = \frac{\sum_i^m1:\{y^{(i)}=1\}x^{(i)}}{\sum_i^m 1:\{y^{(i)}=1\}}
\end{equation}
Which is estimating mean parameter of multivariate Gaussian distribution from data. It only calculates mean of data vectors $x$
for which class is $y=1$.\\
\begin{equation}
\Sigma = \frac{1}{m}(x-\mu)(x-\mu)^T
\end{equation}
Which is estimating covariance matrix from data. Values $x$ and $\mu$ are for both classes.\\
So result is two Gaussian distributions with different means (center of data classes) and they share covariance matrix.\\
After fitting parameters we use equation \ref{eq:argmax} which is returning target value of highest probability. Also, 
prior probability can be uniform so we only use likelihood.\\

\subsection{Connection with logistic regression}
After we model likelihood $p(x|y)$ with Gaussian and using this we model posterior $p(y|x)$ we can plot values
of $p(y=1|x)$ for variety of examples $x$. Plot will look like logistic function.\\
Formally if we plot function $p(y|x;\mu_1, \mu2, \Sigma, \phi)$ as function of $x$ we can express its form as
$\frac{1}{1+exp(-\theta^Tx)}$. Where $\theta$ is function of $\phi, \mu_1, \mu_2, \Sigma$.\\
This shows that if we model likelihood $p(x|y)$ with Gaussian (this must be property of data we have) then this 
will follow in logistic regression. Vice versa is not true. That means that Gaussian discriminative model has 
strong assumption: data is generated by some Gaussian distribution.\\
Also if we model likelihood $p(x|y)$ with Poisson or any other exponential family distribution its posterior
will result in logistic regression. That means:
\begin{itemize}
\item Gaussian discriminant analysis (or any other exponential family distribution in generative models) is having
		strong assumption about data. If assumption is true GDA will preform better in terms of accuracy. It 
		doesn't requires a lot of data because of assumption.
\item Logistic regression is result from any distribution from exponential family so it requires lots of data
		because no assumption is made. Lot of data is making logistic regression more sure of data distribution.
\end{itemize}


\section{Naive Bayes}
This is also generative algorithm. But with additional assumption on likelihood.\\
In Gaussian discriminant analysis (GDA) feature vectors $x$ were continuous valued. So to model likelihood $p(x|y)$ we
used Gaussian distribution.\\
In this section we introduce idea how to model feature vectors $x$ that are discrete. So, we can model likelihood
$p(x|y)$ using multinomial distribution.\\
Problem with modelling the likelihood using multinomial distribution is when we have large feature vectors. For example
if we do the spam classification:
\begin{itemize}
\item[1.] Create dictionary from training set which includes all mails that are labelled as spam/non spam. Dictionary
			contains lot of words (e.g. 50000)
\item[2.] For every mail check if word from dictionary exists. So, every mail will be represented with vector of 
			$0$ and $1$. Where position $i$ will contain $1$ if word $i$ is in mail.
\end{itemize}
This model is called multivariate Bernoulli. Which means that we have more then one Bernoulli RV.\\
So, we can see that feature vectors are large. So, parameter vector will be too large ($2^{50000}$ elements).\\
That is why we introduce (naive) assumption while modelling likelihood $p(x|y)$. Assumption is that feature 
elements $x_i$ are conditionally independent given $y$. This we write as:
\begin{equation}
\begin{split}
p(x_1,...,x_n) = \\
p(x_1|y)p(x_2|y,x_1)...p(x_n|y,x_1,...,x_{n-1}) = \\
naive \quad assumption \\
= p(x_1|y)...p(x_n|y) = \\
= \prod_i^n p(x_i|y)
\end{split}
\end{equation}
\textbf{NB:} even with this strong assumption Naive Bayes works well in practice.\\
Now, value $p(x_i|y)$ is modelled with Bernoulli distribution because we assume feature vectors are binary vectors\\
As for prior $p(y)$ we model it with Bernoulli distribution.\\
This model is parametrized with:
\begin{equation}
\begin{split}
\phi_{i|y=1} = p(x_i=1|y=1) \\
\phi_{i|y=0} = p(x_i=1|y=0)
\end{split}
\end{equation}
Which are parameters for likelihood probability.
\begin{equation}
\phi_{y} = p(y=1)
\end{equation}
Which is parameter for prior.\\
Now, same as before we write joint likelihood:
\begin{equation}
L(\phi_{i|y=1}, \phi_{i|y=0}, \phi_{y}) = \prod_i^m p(x^{(i)}, y^{(i)})
\end{equation}
Maximizing likelihood with respect to $\phi_{i|y=1}, \phi_{i|y=0}, \phi_{y}$ we get estimates for parameters
using data:
\begin{itemize}
\item $\phi_{i|y=1}$ is fraction of examples with label $y=1$ where $i-th$ feature element appears.
\item $\phi_{i|y=0}$ is fraction of examples with label $y=0$ where $i-th$ feature element appears.
\item $\phi_{y}$ fraction of examples with label $y=1$
\end{itemize}
For new example $x$ we use posterior $p(y|x)$ with naive assumption.
\textbf{NB:} if feature vector is not binary then likelihood $p(x_i|y)$ is modelled with multinomial distribution.\\
\textbf{NB:} if some features are continuous use discretization (intervals). This approach sometimes can be better than
			modelling likelihood using Gaussian distribution.

\subsection{Laplace smoothing}
If new emails contain important keywords that are not in dictionary then likelihood will be zero. Prediction will not
be able to be done.\\
Idea is to add $1$ to nominator and $2$ to denominator of fraction $\phi_{i|y=1}, \phi_{i|y=0}$. This is called 
\textbf{Laplace smoothing}
		
\subsection{Advanced model for text classification}
This first approach is using Naive Bayes with multivariate Bernoulli event model.\\
Advanced model uses naive Bayes with multinomial event model. -TODO\\
So, every email is feature vector. Vector has $n_i$ elements where $n_i$ is number of words in $i-th$ email.
 Every element of feature vector $x_j$ can be word from dictionary. So,
value of $x_j$ is no longer binary but it has $k$ classes where $k$ is number of words in dictionary.\\
TODO...\\



\end{document}

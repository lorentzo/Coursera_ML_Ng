\contentsline {section}{\numberline {1}Generative VS discriminative learning algorithms}{2}
\contentsline {section}{\numberline {2}Gaussian discriminant analysis}{2}
\contentsline {subsection}{\numberline {2.1}About multivariate Gaussian}{2}
\contentsline {subsection}{\numberline {2.2}Gaussian discriminant analysis model}{3}
\contentsline {subsection}{\numberline {2.3}Connection with logistic regression}{4}
\contentsline {section}{\numberline {3}Naive Bayes}{4}
\contentsline {subsection}{\numberline {3.1}Laplace smoothing}{6}
\contentsline {subsection}{\numberline {3.2}Advanced model for text classification}{6}

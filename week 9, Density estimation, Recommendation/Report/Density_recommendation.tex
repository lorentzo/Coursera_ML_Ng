\documentclass[10pt]{article}

\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {./figures/} }

\usepackage{amsmath}
\usepackage{amssymb}
 
\begin{document}

\textbf{week 9. Anomaly detection. Recommendation systems}

\section{Anomaly detection}

\subsection{Density estimation}

\textbf{Motivation.}\\
Used for anomaly detection. Unsupervised learning.\\
Given non-anomaly examples $x^{(i)}$, $i=1..m$, build a model $p(x)$.\\
For new example $x$ if $p(X) < \epsilon$ then $x$ is a anomaly.\\
Application in suspicious behavior, manufacturing (suspicious product), monitoring.\\
\\
\textbf{Gaussian distribution}\\
Random variable $X$ is normally distributed if $X~N(\mu, \sigma^2)$.\\
Probability of value $x$ resulting from random variable $X$ is $p(x;\mu, \sigma^2)$.\\
Normal distribution is defined with parameter $\mu$ which is estimated from data (sample):
\begin{equation}
\mu = \frac{1}{m} \sum_{i}^{m} x^{(i)}
\label{eq:mu1}
\end{equation}
And parameter $\sigma^2$. Which is estimated:
\begin{equation}
\sigma^2 = \frac{1}{m}\sum_i^m(x^{(i)}-\mu)^2
\label{eq:sigma1}
\end{equation}
This is example of data with one feature ($n=1$).\\
\\
\textbf{Algorithm}
Given data $x^{(i)}$, $i=1..m$, where $x^{(i)}$ is $n$ dimensional vector we calculate model:
\begin{equation}
\begin{split}
p(x) = p(x_1;\mu_1, \sigma_1^2) ... p(x_1;\mu_n, \sigma_n^2) \\
     = \prod_{j=1}^{n}p(x_j; \mu_j, \sigma_j^2) \\
Where,\\
x_1 = N(\mu_1, \sigma_1^2)\\
...\\
x_n = N(\mu_n, \sigma_n^2)\\
\end{split}
\label{eq:density}
\end{equation}
This is called problem of density estimation. Assumption is that features are independent. Although 
algorithm will work fine without this assumption. Independent features have no correlation.\\
Steps for density estimation:
\begin{itemize}
\item[1] choose features $x_j$, $j=1...n$ that would be indicative of anomaly. 
\item[2] for every feature $j$ given $m$ examples estimate parameters $\mu_j$ and $\sigma_j^2$ using:
\begin{equation}
\mu = \frac{1}{m} \sum_{i}^{m} x^{(i)}
\label{eq:mun}
\end{equation}
\begin{equation}
\sigma^2 = \frac{1}{m}\sum_{i=1}^m(x^{(i)}-\mu)^2
\label{eq:sigman}
\end{equation}
Where $\mu$ and $x^{(i)}$ are vectors of $n$ elements.
\item[3] Given new example $x$ calculate equation \ref{eq:density}. If $p(x) < \epsilon$ then $x$ is 
an anomaly.
\end{itemize} 

\subsection{Anomaly detection system}

\textbf{Developing and evaluating}\\
While developing algorithm (e.g. choosing features) we need to define evaluation metrics.\\
Assume we have data labeled as anomaly/non anomaly, $y=1/y=0$. Then separate data in:
\begin{itemize}
\item \textbf{Training set.} This set will contain around 60\% of all non-anomaly examples, $y=0$.
\item \textbf{Validation set.} This set will contain 20\% non-anomaly examples ($y=0$) and 50\% of anomaly
		examples ($y=1$).
\item \textbf{Test set.} This set will contain 20\% non-anomaly examples ($y=0$) and 50\% of anomaly
		examples ($y=1$).
\end{itemize}
Now, steps:
\begin{itemize}
\item estimate parameters of model $p(x)$ on train set (non-anomaly examples)
\item on validation/test set predict:
\begin{equation}
\begin{split}
y = 1, if p(x) < \epsilon
y = 0, if p(x) > \epsilon
\end{split}
\end{equation}
\item to evaluate algorithm use metric. This is as classification problem so 
		metrics as accuracy, precision, recall and $F_1$ ca be used. Classes
		are skewed (lot of non-anomaly, $y=0$, examples) so use $F_1$ metric.
\item if we want to choose $\epsilon$ then use validation set. After evaluate 
		on test set.
\end{itemize}

\textbf{Supervised learning vs anomaly detection}\\
We assumed we have labeled data.\\
Use \textbf{anomaly detection} if:
\begin{itemize}
\item small number of positive examples (anomaly examples, $y=1$) and large 
	  number of negative examples (non-anomaly, $y=0$) - problem of skewed classes,
	  $p(x)$ can be fitted very well nevertheless.
\item many different types of anomalies (hard to learn from small number of positive
		examples in supervised learning)
\item future anomalies may not look like any anomalies seen bf far
\end{itemize}
Use \textbf{supervised learning} if:
\begin{itemize}
\item large number of positive and negative examples
\item enough positive example to learn what positive examples look like
\item future positive examples similar
\end{itemize}

\textbf{Choosing features}\\
If plotting data gives distribution (histogram) that doesn't look like Gaussian
then preform data transformations:
\begin{itemize}
\item use $log(x_i + x)$ where $c$ is parameter we choose.
\item use $x_i^c$ where $c$ can result in squaring or powering feature $x_i$
\end{itemize}
\textbf{Error analysis}. We want $p(x)$ large for normal examples and small $p(x)$ 
for anomalies. Problem we may encounter is in figure \ref{im:newfeat}.
\begin{figure}[H]
\includegraphics[scale=0.5]{newfeat}
\centering
\caption{Adding features (e.g. $x_2=x_1^2$) can reveal anomalies.}
\label{im:newfeat}
\end{figure}
So, introducing new features that are taking unusually large or small values or using 
combination of existing ones can give better perspective and result in finding anomalies.\\

\subsection{Multivariate Gaussian}
If we model:

\begin{figure}[H]
\includegraphics[scale=0.5]{independentModel}
\centering
\caption{Modeling density distribution with $p(x_1,x_2) = p(x_1)p(x_2)$ means that 
		we assume that features are independent (no correlation)}.
\label{im:independentModel}
\end{figure}

Assuming independent features sometimes can't give good anomaly detection.\\
Idea is to model multiple features with multivariate Gaussian. In this case
parameters are:
\begin{itemize}
\item mean $\mu$ which is $n$ dimensional vector
\item co variance matrix $\Sigma$ which is $nxn$ matrix
\end{itemize}
Now we model feature as: $p(x;\mu,\Sigma)$ which is multivariate Gaussian.\\
\textbf{Note:} value $|\sigma|$ is determinant.\\
With multivariate Gaussian we model covariances between features $x_ix_i$ which is
variance of feature $x_i$ and covariances between features $x_ix_j$. So, covariance 
matrix on diagonal has variances of every feature and other elements are covariances
between features.\\
Examples of different covariance matrices:
\begin{itemize}
\item Diagonal, same elements
\begin{equation}
\begin{bmatrix}
1 & 0 \\
0 & 1 \\
\end{bmatrix}
\end{equation}
\begin{figure}[H]
\includegraphics[scale=0.5]{diag11}
\centering
\label{im:diag11}
\end{figure}
\item Diagonal, different elements
\begin{equation}
\begin{bmatrix}
1 & 0 \\
0 & 0.5 \\
\end{bmatrix}
\end{equation}
\begin{figure}[H]
\includegraphics[scale=0.5]{diag105}
\centering
\label{im:diag105}
\end{figure}
\item Full matrix. Diagonals are equal numbers. Covariance between features are equal.
\begin{equation}
\begin{bmatrix}
1 & 0.5 \\
0.5 & 1 \\
\end{bmatrix}
\end{equation}
\begin{figure}[H]
\includegraphics[scale=0.5]{full}
\centering
\label{im:full}
\end{figure}
\end{itemize}
In this way estimation of parameters will give best Gaussian for anomalies.\\
\\
\textbf{Anomaly detection with multivariate Gaussian}\\
Given dataset $x^{(i})$, where $i=1..m$. Estimate parameters:
\begin{itemize}
\item[1.] Mean:
\begin{equation}
\mu = \frac{1}{m}\sum_{i=1}^m x^{(i)}
\end{equation}
\item[2.] Covariance matrix:
\begin{equation}
\Sigma = \frac{1}{m}\sum_{i=1}^{m}(x^{(i)}-\mu)(x^{(i)}-\mu)^T
\end{equation}
\end{itemize}
For new example $x$ calculate $p(x;\mu,\Sigma)$ which is density of multivariate Gaussian.
If $p(x;\mu,\Sigma)<\epsilon$ then $x$ is anomaly.\\
\\
Original density estimation $p(x) = \prod_{j=1}^{n}p(x_j; \mu_j, \sigma_j^2)$ is special case
of multivariate Gaussian where co variance matrix is diagonal and contains only variances 
between features and covariances between features are 0.\\
\\
Original density estimation vs multivariate density estimation:
\begin{itemize}
\item In original density estimation we manually need to find features that will
		capture anomaly as in figure \ref{im:newfeat} so combination we manually need
		to find should capture anomalies.
\item Multivariate Gaussian captures correlation between features. In that way 
		unusual combinations are detected automatically
\item Multivariate Gaussian estimation is computationally expensive
\item Co variance matrix $\Sigma$ is non invertible if $m<n$, so we use multivariate
		for large number of examples. Original estimation is used when $m$ is small
\item original estimation is fast and it scales up good with lot of features.
\end{itemize}
\textbf{Note:} $\sigma$ is not invertible if:
\begin{itemize}
\item $m<n$
\item We have redundant features. This means we have columns that are lineary dependent. 
		Redundant features must be eliminated.
\end{itemize}

\section{Recommendation systems}

\subsection{Prediction of ratings}
\textbf{Problem formulation.}\\
Given matrix $Y$, dimensions $n_m x n_u$, where $n_m$ is number of products and $n_u$ is number of users
we need to predict missing ratings of users for products.\\
We denote $r(i,j) = 1$ if user $j$ has rated product $i$. Also, $y^{(i,j)}$ denotes which score has
user $j$ given to product $i$.\\
\\
\textbf{Content based recommendation}\\
This approach is using features of products (contents).Every product $i$ is assigned a feature 
vector $x^{(i)}$.\\
Approach is to treat prediction of every user as separate linear regression problem. For each 
user we need to learn parameters $\theta^{(j)}$.\\
Prediction of rating for every user is $(\theta^{(j)})^Tx^{(i)}$.\\
To learn parameters $\theta^{(j)}$ for every user $j$ we use:
\begin{equation}
min_{\theta^{(1)},...,\theta^{(n_u)}} \frac{1}{2} \sum_{j=1}^{n_u} \sum_{i:r(i,j)=1}
( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} )^2 + \frac{\lambda}{2} \sum_{j=1}^{n_u} \sum_{k=1}^{n}(\theta_k^{(j)})^2
\label{eq:ContBasedCost}
\end{equation}
We need to minimize this cost function using gradient descent or any advanced
method. Gradient descend is:
\begin{equation}
\begin{split}
\theta_{k}^{(j)} := \theta_{k}^{(j)} - \alpha \sum_{i:r(i,j)=1}
((\theta^{(j)})^Tx^{(i)}-y^{(i,j)})x_k^{(i)}, \quad for \quad k=0 \\
\theta_{k}^{(j)} := \theta_{k}^{(j)} - \alpha (\sum_{i:r(i,j)=1}
((\theta^{(j)})^Tx^{(i)}-y^{(i,j)})x_k^{(i)} + \lambda\theta_k^{(j)}), \quad for \quad k>0
\end{split}
\end{equation}

\subsection{Collaborative filtering}

\textbf{problem motivation}\\
This approach is doing feature learning. Same data matrix as before is given.\\
Assume that we know user preferences for products, then we know $\theta^{(j)}$. Now
we can infer product features using ratings from matrix and user preferences for products 
of certain types:
\[
(\theta^{(j)})^T x^{(i)} \approx rating
\]
Formal, given $\theta^{(1)}...\theta^{(n_u)}$ learn $x{(i)}$, $i=1..n_m$:
\begin{equation}
min_{x^{(1)},...,x^{(n_m)}} \frac{1}{2} \sum_{j=1}^{n_m} \sum_{j:r(i,j)=1}
( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} )^2 + \frac{\lambda}{2} \sum_{j=1}^{n_m} \sum_{k=1}^{n}(x_k^{(j)})^2
\label{eq:collabfiltcost1}
\end{equation}
\\
\textbf{Algorithm}\\
Using equations \ref{eq:ContBasedCost} and \ref{eq:collabfiltcost1} we can given
$x^{(i)}$, $i=1..n_m$ calculate $\theta^{(j)}$, $j=1..n_u$ (equation \ref{eq:ContBasedCost}). Also we can calculate $x^{(i)}$, $i=1..n_m$
given $\theta^{(j)}$, $j=1..n_u$ (equation \ref{eq:collabfiltcost1}).\\
So, idea is to minimize both objectives:
\begin{equation}
\begin{split}
min_{x^{(1)}..x^{(n_m), \theta^{(1)}..\theta^{(n_u)}}} J(\vec{x}, \vec{\theta}) \\
J(\vec{x}, \vec{\theta}) = \frac{1}{2}\sum_{(i,j):r(i,j)=1}( (\theta^{(j)})^T x^{(i)} - y^{(i,j)} )^2 \\
						+ \frac{\lambda}{2} \sum_{j=1}^{n_u} \sum_{k=1}^{n}(\theta_k^{(j)})^2 \\
						+ \frac{\lambda}{2} \sum_{j=1}^{n_m} \sum_{k=1}^{n}(x_k^{(j)})^2
\end{split}
\label{eq:collabfilt2}
\end{equation}

This equation is just equations \ref{eq:ContBasedCost} and \ref{eq:collabfiltcost1} in one.\\
So, steps are:
\begin{itemize}
\item Initialize $\theta^{(j)}$, $j=1..n_u$ and $x^{(i)}$, $i=1..n_m$ to
		some small value. This is used for symmetry breaking.
\item Minimize $J(\vec{x}, \vec{\theta})$ from equation \ref{eq:collabfilt2}. Using gradient descend or advanced method:
\begin{equation}
\begin{split}
\theta_{k}^{(j)} := \theta_{k}^{(j)} - \alpha (\sum_{i:r(i,j)=1}
((\theta^{(j)})^Tx^{(i)}-y^{(i,j)})x_k^{(i)} + \lambda\theta_k^{(j)}) \\
x_{k}^{(i)} := x_{k}^{(i)} - \alpha (\sum_{j:r(i,j)=1}
((\theta^{(j)})^Tx^{(i)}-y^{(i,j)})\theta_k^{(j)} + \lambda x_k^{(i)})
\end{split}
\end{equation}
\item For user with parameters $\theta$ and product with (learned) features
$x$, predict rating of $\theta^Tx$
\end{itemize}

\textbf{Low rank matrix factorization}\\
Matrix with product-user ratings can be shown as:
\begin{equation}
\begin{bmatrix}
(\theta^{(1)})^T x^{(1)} \quad ... \quad \theta^{(n_u)})^T x^{(1)} \\
	... \quad ... \quad ... \quad ... \\
(\theta^{(1)})^T x^{(n_m)} \quad ... \quad \theta^{(n_u)})^T x^{(n_m)}
\end{bmatrix}
\end{equation}
Which can be written as multiplication of:
\begin{equation}
\begin{bmatrix}
---(x^{(1)})^T --- \\
	... \quad ... \quad ... \quad ... \\
---(x^{(n_m)})^T ---
\end{bmatrix}
\end{equation}
And:
\begin{equation}
\begin{bmatrix}
---(\theta^{(1)})^T --- \\
	... \quad ... \quad ... \quad ... \\
---(\theta^{(n_u)})^T ---
\end{bmatrix}
\end{equation}
Which is called low rank matrix factorization.\\
\\
\textbf{Related products}
for each product $i$ we can learn feature vector $x^{(i)}$. Using 
this we can calculate how product $i$ is related to product $j$ using:
\begin{equation}
if \quad small \quad ||x^{(i)} - x^{(j)}|| \quad then \quad similar
\end{equation}

\textbf{Implementation detail.}\\
When we have users with no ratings:
\begin{itemize}
\item calculate mean $\vec{\mu}$ of ratings matrix $Y$ by rows.
\item subtract $\vec{\mu}$ from ratings matrix $Y- \vec{\mu}$
\item when calculating prediction for user $j$ and product $i$
		$(\theta^{(j)})^T (x^{(i)}$ add $\mu_i$
\end{itemize}
In this way when user doesn't have any ratings, user will receive 
rating of $\mu_i$.\\








\end{document}